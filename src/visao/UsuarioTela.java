package visao;
/*
//import controle.UsuarioControle;
import java.awt.Dimension;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import modelo.bean.UsuarioBean;
import modelo.conexao.Conexao;
//import modelo.dao.UsuarioDao;

public final class UsuarioTela extends javax.swing.JInternalFrame {

    UsuarioBean usuarioB;
 //   UsuarioControle usuarioC;
  //  UsuarioDao usuarioD;

    public UsuarioTela() {
        initComponents();
        focarPesquisa();
        habilitarCampos(false);
        usuarioB = new UsuarioBean();
        usuarioC = new UsuarioControle();
   //     usuarioD = new UsuarioDao();
        lerTabelaUsuario();
    }

    @SuppressWarnings("unchecked")

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblTitulo = new javax.swing.JLabel();
        jcbTipoPesquisa = new javax.swing.JComboBox<>();
        jtfPesquisa = new javax.swing.JTextField();
        btnPesquisar = new javax.swing.JButton();
        btnNovo = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtUsuarios = new javax.swing.JTable();
        lblNome = new javax.swing.JLabel();
        jtfNomeGuerra = new javax.swing.JTextField();
        lblSenha = new javax.swing.JLabel();
        jtfSenha = new javax.swing.JPasswordField();
        lblConfirmarSenha = new javax.swing.JLabel();
        lblTipo = new javax.swing.JLabel();
        jcbTipo = new javax.swing.JComboBox<>();
        jtfConfirmarSenha = new javax.swing.JPasswordField();
        btnSalvar = new javax.swing.JButton();
        btnAlterar = new javax.swing.JButton();
        btnDeletar = new javax.swing.JButton();
        btnSair = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setClosable(true);
        setIconifiable(true);
        setTitle("Cadastro de Usuários");
        getContentPane().setLayout(null);

        lblTitulo.setFont(new java.awt.Font("Ubuntu", 1, 24)); // NOI18N
        lblTitulo.setForeground(new java.awt.Color(14, 24, 23));
        lblTitulo.setText("Definição de Permissões");
        getContentPane().add(lblTitulo);
        lblTitulo.setBounds(230, 10, 300, 32);

        jcbTipoPesquisa.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Nome de Guerra", "Tipo" }));
        jcbTipoPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbTipoPesquisaActionPerformed(evt);
            }
        });
        getContentPane().add(jcbTipoPesquisa);
        jcbTipoPesquisa.setBounds(27, 71, 150, 20);

        jtfPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtfPesquisaActionPerformed(evt);
            }
        });
        getContentPane().add(jtfPesquisa);
        jtfPesquisa.setBounds(195, 71, 221, 20);

        btnPesquisar.setText("Pesquisar");
        btnPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPesquisarActionPerformed(evt);
            }
        });
        getContentPane().add(btnPesquisar);
        btnPesquisar.setBounds(426, 69, 126, 25);

        btnNovo.setText("Novo");
        btnNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovoActionPerformed(evt);
            }
        });
        getContentPane().add(btnNovo);
        btnNovo.setBounds(633, 69, 110, 25);

        jtUsuarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "Nome de Guerra", "Tipo"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jtUsuarios.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtUsuariosMouseClicked(evt);
            }
        });
        jtUsuarios.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jtUsuariosKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jtUsuariosKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jtUsuariosKeyTyped(evt);
            }
        });
        jScrollPane1.setViewportView(jtUsuarios);
        if (jtUsuarios.getColumnModel().getColumnCount() > 0) {
            jtUsuarios.getColumnModel().getColumn(0).setResizable(false);
            jtUsuarios.getColumnModel().getColumn(1).setResizable(false);
            jtUsuarios.getColumnModel().getColumn(2).setResizable(false);
        }

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(25, 112, 718, 160);

        lblNome.setText("Nome de Guerra: ");
        getContentPane().add(lblNome);
        lblNome.setBounds(30, 310, 100, 20);

        jtfNomeGuerra.setEnabled(false);
        jtfNomeGuerra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtfNomeGuerraActionPerformed(evt);
            }
        });
        getContentPane().add(jtfNomeGuerra);
        jtfNomeGuerra.setBounds(137, 310, 220, 20);

        lblSenha.setText("Senha:");
        getContentPane().add(lblSenha);
        lblSenha.setBounds(80, 370, 50, 14);

        jtfSenha.setEnabled(false);
        getContentPane().add(jtfSenha);
        jtfSenha.setBounds(137, 367, 220, 20);

        lblConfirmarSenha.setText("Confirmar Senha: ");
        getContentPane().add(lblConfirmarSenha);
        lblConfirmarSenha.setBounds(410, 370, 110, 14);

        lblTipo.setText("Tipo:");
        getContentPane().add(lblTipo);
        lblTipo.setBounds(480, 310, 30, 14);

        jcbTipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "", "Administrador", "Armeiro" }));
        jcbTipo.setEnabled(false);
        jcbTipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbTipoActionPerformed(evt);
            }
        });
        getContentPane().add(jcbTipo);
        jcbTipo.setBounds(520, 310, 220, 20);

        jtfConfirmarSenha.setEnabled(false);
        getContentPane().add(jtfConfirmarSenha);
        jtfConfirmarSenha.setBounds(520, 370, 220, 20);

        btnSalvar.setText("Salvar");
        btnSalvar.setEnabled(false);
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });
        getContentPane().add(btnSalvar);
        btnSalvar.setBounds(153, 419, 110, 25);

        btnAlterar.setText("Alterar");
        btnAlterar.setEnabled(false);
        btnAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAlterarActionPerformed(evt);
            }
        });
        getContentPane().add(btnAlterar);
        btnAlterar.setBounds(273, 419, 100, 25);

        btnDeletar.setText("Excluir");
        btnDeletar.setEnabled(false);
        btnDeletar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeletarActionPerformed(evt);
            }
        });
        getContentPane().add(btnDeletar);
        btnDeletar.setBounds(383, 419, 110, 25);

        btnSair.setText("Sair");
        btnSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSairActionPerformed(evt);
            }
        });
        getContentPane().add(btnSair);
        btnSair.setBounds(503, 419, 110, 25);

        jLabel1.setIcon(new javax.swing.ImageIcon("E:\\Documentos\\NetBeansProjects\\projeto_sagapom\\imagens\\telaprincipal.png")); // NOI18N
        getContentPane().add(jLabel1);
        jLabel1.setBounds(0, 0, 780, 60);

        jLabel2.setIcon(new javax.swing.ImageIcon("E:\\Documentos\\NetBeansProjects\\projeto_sagapom\\imagens\\telaprincipal.png")); // NOI18N
        getContentPane().add(jLabel2);
        jLabel2.setBounds(0, 460, 790, 10);

        setBounds(0, 0, 792, 500);
    }// </editor-fold>//GEN-END:initComponents

    private void jcbTipoPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbTipoPesquisaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jcbTipoPesquisaActionPerformed

    private void jtfPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtfPesquisaActionPerformed
    }//GEN-LAST:event_jtfPesquisaActionPerformed

    private void btnPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPesquisarActionPerformed
        if (jcbTipoPesquisa.getSelectedItem() == "Nome de Guerra") {
            lerTabelaPorNome(jtfPesquisa.getText());
        } else if (jcbTipoPesquisa.getSelectedItem() == "Tipo") {
            lerTabelaPorTipo(jtfPesquisa.getText());
        }
    }//GEN-LAST:event_btnPesquisarActionPerformed

    private void btnNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovoActionPerformed
        limparDados();
        habilitarCampos(true);
        habilitarBotoes(true, true, false, false);
    }//GEN-LAST:event_btnNovoActionPerformed

    private void jtUsuariosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtUsuariosMouseClicked
        PreparedStatement ps;
        ResultSet rs;
        Connection con;
        String sql;

        try {
            con = Conexao.pegarConexao();
            int row = jtUsuarios.getSelectedRow();
            String table_click = (jtUsuarios.getModel().getValueAt(row, 0).toString());
            sql = "select * from usuario where usu_cod='" + table_click + "' ";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                String add1 = rs.getString("usu_nome_guerra");
                jtfNomeGuerra.setText(add1);
                String add2 = rs.getString("usu_tipo");
                jcbTipo.setSelectedItem(add2);
                String add3 = rs.getString("usu_senha");
                jtfSenha.setText(add3);
                jtfConfirmarSenha.setText(add3);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
        habilitarCampos(true);
        habilitarBotoes(true, false, true, true);
    }//GEN-LAST:event_jtUsuariosMouseClicked

    private void jtUsuariosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtUsuariosKeyPressed

    }//GEN-LAST:event_jtUsuariosKeyPressed

    private void jtUsuariosKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtUsuariosKeyReleased

    }//GEN-LAST:event_jtUsuariosKeyReleased

    private void jtUsuariosKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtUsuariosKeyTyped

    }//GEN-LAST:event_jtUsuariosKeyTyped

    private void jtfNomeGuerraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtfNomeGuerraActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtfNomeGuerraActionPerformed

    private void jcbTipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbTipoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jcbTipoActionPerformed

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        if (jtfNomeGuerra.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Preencha o campo 'Usuário' para continuar!");
            jtfNomeGuerra.requestFocus();
        } else if (jtfSenha.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Preencha o campo 'Senha' para continuar!");
            jtfSenha.requestFocus();
        } else if (jtfConfirmarSenha.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Preencha o campo 'Confirmar Senha' para continuar!");
            jtfConfirmarSenha.requestFocus();
        } else {
            String t1 = jtfSenha.getText();
            String t2 = jtfConfirmarSenha.getText();
            if ((t1.equals(t2))) {
                popularUsuarioBean();
                usuarioC.salvarUsuario(usuarioB);
                lerTabelaUsuario();
                limparDados();
                habilitarCampos(false);
                habilitarBotoes(true, false, false, false);
                focarPesquisa();
            } else {
                JOptionPane.showMessageDialog(null, "As senhas não correspondem!");
            }
        }
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void btnAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAlterarActionPerformed
        if (jtfNomeGuerra.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Preencha o campo 'Usuário' para continuar!");
            jtfNomeGuerra.requestFocus();
        } else if (jtfSenha.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Preencha o campo 'Senha' para continuar!");
            jtfSenha.requestFocus();
        } else if (jtfConfirmarSenha.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Preencha o campo 'Confirmar Senha' para continuar!");
            jtfConfirmarSenha.requestFocus();
        } else {
            String t1 = jtfSenha.getText();
            String t2 = jtfConfirmarSenha.getText();
            if ((t1.equals(t2))) {
                int resposta = 0;
                resposta = JOptionPane.showConfirmDialog(rootPane, "Deseja realmente alterar?");
                if (resposta == JOptionPane.YES_OPTION) {
                    popularUsuarioBean();
                    usuarioB.setCodigo((int) jtUsuarios.getValueAt(jtUsuarios.getSelectedRow(), 0));
                    usuarioC.alterarUsuario(usuarioB);
                    lerTabelaUsuario();
                    limparDados();
                    habilitarCampos(false);
                    habilitarBotoes(true, false, false, false);
                    focarPesquisa();
                }
                } else {
                    JOptionPane.showMessageDialog(null, "As senhas não correspondem!");
                }
            }
    }//GEN-LAST:event_btnAlterarActionPerformed

    private void btnDeletarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeletarActionPerformed
        int resposta = 0;
        resposta = JOptionPane.showConfirmDialog(rootPane, "Deseja realmente excluir?");
        if (resposta == JOptionPane.YES_OPTION) {
            usuarioB.setCodigo((int) jtUsuarios.getValueAt(jtUsuarios.getSelectedRow(), 0));
            usuarioC.excluirUsuario(usuarioB);
            lerTabelaUsuario();
            limparDados();
            habilitarCampos(false);
            habilitarBotoes(true, false, false, false);
        }
    }//GEN-LAST:event_btnDeletarActionPerformed

    private void btnSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSairActionPerformed
        dispose();
    }//GEN-LAST:event_btnSairActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAlterar;
    private javax.swing.JButton btnDeletar;
    private javax.swing.JButton btnNovo;
    private javax.swing.JButton btnPesquisar;
    private javax.swing.JButton btnSair;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JComboBox<Object> jcbTipo;
    private javax.swing.JComboBox<String> jcbTipoPesquisa;
    private javax.swing.JTable jtUsuarios;
    private javax.swing.JPasswordField jtfConfirmarSenha;
    private javax.swing.JTextField jtfNomeGuerra;
    private javax.swing.JTextField jtfPesquisa;
    private javax.swing.JPasswordField jtfSenha;
    private javax.swing.JLabel lblConfirmarSenha;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblSenha;
    private javax.swing.JLabel lblTipo;
    private javax.swing.JLabel lblTitulo;
    // End of variables declaration//GEN-END:variables
    public final void habilitarCampos(boolean valor) {
        jtfNomeGuerra.setEnabled(valor);
        jcbTipo.setEnabled(valor);
        jtfSenha.setEnabled(valor);
        jtfConfirmarSenha.setEnabled(valor);
    }

    public void habilitarBotoes(boolean N, boolean S, boolean C, boolean D) {
        btnNovo.setEnabled(N);
        btnSalvar.setEnabled(S);
        btnAlterar.setEnabled(C);
        btnDeletar.setEnabled(D);
    }

    public void limparDados() {
        jtfNomeGuerra.setText("");
        jcbTipo.setSelectedIndex(0);
        jtfSenha.setText("");
        jtfConfirmarSenha.setText("");
    }

    public void setPosicao() {
        Dimension d = this.getDesktopPane().getSize();
        this.setLocation((d.width - this.getSize().width) / 2, (d.height - this.getSize().height) / 2);
    }

    public final void focarPesquisa() {
        final JTextField ftf = jtfPesquisa;
        javax.swing.SwingUtilities.invokeLater(ftf::requestFocusInWindow);
    }

    public void popularUsuarioBean() {
   //     usuarioB.setNome_guerra(jtfNomeGuerra.getText());
   //     usuarioB.setTipo((String) jcbTipo.getSelectedItem());
        usuarioB.setSenha(jtfSenha.getText());
        usuarioB.setSenha(jtfConfirmarSenha.getText());
    }

    public void lerTabelaUsuario() {
        DefaultTableModel dtmUsuarios = (DefaultTableModel) jtUsuarios.getModel();
        dtmUsuarios.setNumRows(0);
        usuarioC.listarTodos().forEach((usuario) -> {
            dtmUsuarios.addRow(new Object[]{
                usuario.getCodigo(),
     //           usuario.getNome_guerra(),
     //           usuario.getTipo()
            });
        });
    }

    public void lerTabelaPorNome(String name) {
        DefaultTableModel dtmUsuarios = (DefaultTableModel) jtUsuarios.getModel();
        dtmUsuarios.setNumRows(0);
        usuarioC.listarPorNome(name).forEach((user) -> {
            dtmUsuarios.addRow(new Object[]{
                user.getCodigo(),
     //           user.getNome_guerra(),
      //          user.getTipo()
            });
        });
    }

    public void lerTabelaPorTipo(String type) {
        DefaultTableModel dtmUsuarios = (DefaultTableModel) jtUsuarios.getModel();
        dtmUsuarios.setNumRows(0);
        usuarioC.listarPorTipo(type).forEach((user) -> {
            dtmUsuarios.addRow(new Object[]{
                user.getCodigo(),
       //         user.getNome_guerra(),
       //         user.getTipo()
            });
        });
    }

}
*/