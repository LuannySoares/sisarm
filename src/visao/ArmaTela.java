package visao;

import java.awt.Dimension;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.bean.ArmaBean;
import modelo.conexao.Conexao;
import modelo.dao.ArmaDao;

public class ArmaTela extends javax.swing.JInternalFrame {

    ArmaBean arma = new ArmaBean();
    ArmaDao armaDao = new ArmaDao();

    public ArmaTela() {
        initComponents();
        listarTodas();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jcbTipoPesquisa = new javax.swing.JComboBox<>();
        jtfPesquisa = new javax.swing.JTextField();
        btnPesquisar = new javax.swing.JButton();
        btnNovo = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtArma = new javax.swing.JTable();
        btnSalvar = new javax.swing.JButton();
        btnAlterar = new javax.swing.JButton();
        btnDesativar = new javax.swing.JButton();
        btnSair = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        lblDescricao = new javax.swing.JLabel();
        jtfDescricao = new javax.swing.JTextField();
        lblMarca = new javax.swing.JLabel();
        jtfMarca = new javax.swing.JTextField();
        lblModelo = new javax.swing.JLabel();
        jtfModelo = new javax.swing.JTextField();
        lblCalibre = new javax.swing.JLabel();
        jtfCalibre = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        lblTitulo = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jtfQuantidade = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jtfNumSerie = new javax.swing.JTextField();

        jcbTipoPesquisa.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Nome", "Calibre" }));
        jcbTipoPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbTipoPesquisaActionPerformed(evt);
            }
        });

        jtfPesquisa.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtfPesquisaMouseClicked(evt);
            }
        });
        jtfPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtfPesquisaActionPerformed(evt);
            }
        });

        btnPesquisar.setText("Pesquisar");
        btnPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPesquisarActionPerformed(evt);
            }
        });

        btnNovo.setText("Novo");
        btnNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovoActionPerformed(evt);
            }
        });

        jtArma.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "Nome", "Marca", "Modelo", "Calibre", "Status"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jtArma.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtArmaMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jtArmaMouseEntered(evt);
            }
        });
        jtArma.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jtArmaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jtArmaKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jtArmaKeyTyped(evt);
            }
        });
        jScrollPane1.setViewportView(jtArma);

        btnSalvar.setText("Salvar");
        btnSalvar.setEnabled(false);
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        btnAlterar.setText("Alterar");
        btnAlterar.setEnabled(false);
        btnAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAlterarActionPerformed(evt);
            }
        });

        btnDesativar.setText("Excluir");
        btnDesativar.setEnabled(false);
        btnDesativar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDesativarActionPerformed(evt);
            }
        });

        btnSair.setText("Sair");
        btnSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSairActionPerformed(evt);
            }
        });

        jLabel2.setIcon(new javax.swing.ImageIcon("E:\\Documentos\\NetBeansProjects\\projeto_sagapom\\imagens\\telaprincipal.png")); // NOI18N

        lblDescricao.setText("Descrição:");

        jtfDescricao.setEnabled(false);
        jtfDescricao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtfDescricaoActionPerformed(evt);
            }
        });

        lblMarca.setText("Marca:");

        jtfMarca.setEnabled(false);
        jtfMarca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtfMarcaActionPerformed(evt);
            }
        });

        lblModelo.setText("Modelo:");

        jtfModelo.setEnabled(false);

        lblCalibre.setText("Calibre:");

        jtfCalibre.setEnabled(false);

        jLabel1.setIcon(new javax.swing.ImageIcon("E:\\Documentos\\NetBeansProjects\\projeto_sagapom\\imagens\\telaprincipal.png")); // NOI18N

        lblTitulo.setFont(new java.awt.Font("Ubuntu", 1, 24)); // NOI18N
        lblTitulo.setForeground(new java.awt.Color(14, 24, 23));
        lblTitulo.setText("Cadastro de Arma");

        jLabel3.setText("Quantidade: ");

        jtfQuantidade.setEnabled(false);
        jtfQuantidade.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtfQuantidadeActionPerformed(evt);
            }
        });

        jLabel4.setText("Número de Série:");

        jtfNumSerie.setEnabled(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblDescricao)
                            .addComponent(lblModelo))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jtfModelo)
                                .addGap(39, 39, 39)
                                .addComponent(lblCalibre)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jtfCalibre, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel3)
                                .addGap(26, 26, 26)
                                .addComponent(jtfQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(btnSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(btnAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(btnDesativar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(4, 4, 4)
                                .addComponent(jtfDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(lblMarca)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jtfMarca, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(24, 24, 24)
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jtfNumSerie, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(jcbTipoPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jtfPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 624, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 662, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 25, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(229, 229, 229)
                .addComponent(lblTitulo)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(lblTitulo))
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jcbTipoPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtfPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDescricao)
                    .addComponent(jtfDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblMarca)
                    .addComponent(jtfMarca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(jtfNumSerie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(42, 42, 42)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtfCalibre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblCalibre)
                    .addComponent(jtfModelo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblModelo)
                    .addComponent(jLabel3)
                    .addComponent(jtfQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(34, 34, 34)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDesativar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 9, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jcbTipoPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbTipoPesquisaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jcbTipoPesquisaActionPerformed

    private void jtfPesquisaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtfPesquisaMouseClicked
        //   habilitarCampos(false);
        //  limparDados();
    }//GEN-LAST:event_jtfPesquisaMouseClicked

    private void jtfPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtfPesquisaActionPerformed

    }//GEN-LAST:event_jtfPesquisaActionPerformed

    private void btnPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPesquisarActionPerformed
        if (jcbTipoPesquisa.getSelectedItem() == "Nome") {
            listarPorNome(jtfPesquisa.getText());
        } else if (jcbTipoPesquisa.getSelectedItem() == "Calibre") {
            listarPorCalibre(jtfPesquisa.getText());
        }
    }//GEN-LAST:event_btnPesquisarActionPerformed

    private void btnNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovoActionPerformed
        limparDados();
        habilitarCampos(true);
        habilitarBotoes(true, true, false, false);
    }//GEN-LAST:event_btnNovoActionPerformed

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        //Ações
        arma.setDescricao(jtfDescricao.getText());
        arma.setMarca(jtfMarca.getText());
        arma.setModelo(jtfModelo.getText());
        arma.setCalibre(jtfCalibre.getText());
        arma.setNum_serie(jtfNumSerie.getText());
        arma.setQuantidade(Integer.parseInt(jtfQuantidade.getText()));

        armaDao.salvarArma(arma);
        listarTodas();
        limparDados();
        habilitarCampos(false);
        habilitarBotoes(true, false, false, false);
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void btnAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAlterarActionPerformed

        arma.setDescricao(jtfDescricao.getText());
        arma.setMarca(jtfMarca.getText());
        arma.setModelo(jtfModelo.getText());
        arma.setCalibre(jtfCalibre.getText());
        arma.setNum_serie(jtfNumSerie.getText());
        arma.setQuantidade(Integer.parseInt(jtfQuantidade.getText()));
        arma.setCodigo((int) jtArma.getValueAt(jtArma.getSelectedRow(), 0));

        armaDao.alterarArma(arma);
        //Ações
        listarTodas();
        limparDados();
        habilitarCampos(false);
        habilitarBotoes(true, false, false, false);
    }//GEN-LAST:event_btnAlterarActionPerformed

    private void btnDesativarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDesativarActionPerformed
        arma.setCodigo((int) jtArma.getValueAt(jtArma.getSelectedRow(), 0));
        armaDao.excluirArma(arma);
        listarTodas();
        limparDados();
        habilitarCampos(false);
        habilitarBotoes(true, false, false, false);
    }//GEN-LAST:event_btnDesativarActionPerformed

    private void btnSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSairActionPerformed
        dispose();
    }//GEN-LAST:event_btnSairActionPerformed

    private void jtArmaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtArmaKeyTyped

    }//GEN-LAST:event_jtArmaKeyTyped

    private void jtArmaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtArmaKeyReleased
    }//GEN-LAST:event_jtArmaKeyReleased

    private void jtArmaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtArmaKeyPressed

    }//GEN-LAST:event_jtArmaKeyPressed

    private void jtArmaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtArmaMouseClicked
        recuperarDados();
    }//GEN-LAST:event_jtArmaMouseClicked

    private void jtfMarcaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtfMarcaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtfMarcaActionPerformed

    private void jtfDescricaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtfDescricaoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtfDescricaoActionPerformed

    private void jtfQuantidadeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtfQuantidadeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtfQuantidadeActionPerformed

    private void jtArmaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtArmaMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_jtArmaMouseEntered


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAlterar;
    private javax.swing.JButton btnDesativar;
    private javax.swing.JButton btnNovo;
    private javax.swing.JButton btnPesquisar;
    private javax.swing.JButton btnSair;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JComboBox<String> jcbTipoPesquisa;
    private javax.swing.JTable jtArma;
    private javax.swing.JTextField jtfCalibre;
    private javax.swing.JTextField jtfDescricao;
    private javax.swing.JTextField jtfMarca;
    private javax.swing.JTextField jtfModelo;
    private javax.swing.JTextField jtfNumSerie;
    private javax.swing.JTextField jtfPesquisa;
    private javax.swing.JTextField jtfQuantidade;
    private javax.swing.JLabel lblCalibre;
    private javax.swing.JLabel lblDescricao;
    private javax.swing.JLabel lblMarca;
    private javax.swing.JLabel lblModelo;
    private javax.swing.JLabel lblTitulo;
    // End of variables declaration//GEN-END:variables
    public void setPosicao() {
        Dimension d = this.getDesktopPane().getSize();
        this.setLocation((d.width - this.getSize().width) / 2, (d.height - this.getSize().height) / 2);
    }

    public void habilitarBotoes(boolean N, boolean S, boolean C, boolean D) {
        btnNovo.setEnabled(N);
        btnSalvar.setEnabled(S);
        btnAlterar.setEnabled(C);
        btnDesativar.setEnabled(D);
    }

    public void habilitarCampos(boolean valor) {
        jtfDescricao.setEnabled(valor);
        jtfMarca.setEnabled(valor);
        jtfModelo.setEnabled(valor);
        jtfCalibre.setEnabled(valor);
        jtfNumSerie.setEnabled(valor);
        jtfQuantidade.setEnabled(valor);
    }

    public void limparDados() {
        jtfDescricao.setText("");
        jtfMarca.setText("");
        jtfModelo.setText("");
        jtfCalibre.setText("");
        jtfNumSerie.setText("");
        jtfQuantidade.setText("");
    }

    public void listarTodas() {
        DefaultTableModel dtmArmas = (DefaultTableModel) jtArma.getModel();
        dtmArmas.setNumRows(0);

        armaDao.listarTodas().forEach((armeiro) -> {
            dtmArmas.addRow(new Object[]{
                armeiro.getCodigo(),
                armeiro.getDescricao(),
                armeiro.getMarca(),
                armeiro.getModelo(),
                armeiro.getCalibre(),
                armeiro.getStatus() == 1 ? "Disponivel" : "Não Disponível"
            });
        });
    }

    public void listarPorNome(String nome) {
        DefaultTableModel dtmArmas = (DefaultTableModel) jtArma.getModel();
        dtmArmas.setNumRows(0);

        armaDao.listarPorNome(nome).forEach((armeiro) -> {
            dtmArmas.addRow(new Object[]{
                armeiro.getCodigo(),
                armeiro.getDescricao(),
                armeiro.getMarca(),
                armeiro.getModelo(),
                armeiro.getCalibre(),
                armeiro.getStatus() == 1 ? "Disponivel" : "Não Disponível"
            });
        });
    }

    public void listarPorCalibre(String calibre) {
        DefaultTableModel dtmArmas = (DefaultTableModel) jtArma.getModel();
        dtmArmas.setNumRows(0);

        armaDao.listarPorCalibre(calibre).forEach((armeiro) -> {
            dtmArmas.addRow(new Object[]{
                armeiro.getCodigo(),
                armeiro.getDescricao(),
                armeiro.getMarca(),
                armeiro.getModelo(),
                armeiro.getCalibre(),
                armeiro.getStatus() == 1 ? "Disponivel" : "Não Disponível"
            });
        });
    }

    public void recuperarDados() {
        PreparedStatement ps;
        ResultSet rs;
        Connection con;
        String sql;

        try {
            con = Conexao.pegarConexao();
            int row = jtArma.getSelectedRow();
            String table_click = (jtArma.getModel().getValueAt(row, 0).toString());
            sql = "select equ_descricao, equ_qtd, arm_marca, arm_modelo, arm_calibre, arm_num_serie"
                    + " from equipamento"
                    + " inner join  arma on arm_equ_cod = equ_cod"
                    + " where arm_equ_cod='" + table_click + "' ";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                //Dados Básicos
                String add1 = rs.getString("equ_descricao");
                jtfDescricao.setText(add1);

                String add2 = rs.getString("arm_marca");
                jtfMarca.setText(add2);

                String add3 = rs.getString("arm_modelo");
                jtfModelo.setText(add3);

                String add4 = rs.getString("arm_num_serie");
                jtfNumSerie.setText(add4);

                String add5 = rs.getString("arm_calibre");
                jtfCalibre.setText(add5);

                String add6 = rs.getString("equ_qtd");
                jtfQuantidade.setText(add6);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
        habilitarCampos(true);
        habilitarBotoes(true, false, true, true);
    }

}
