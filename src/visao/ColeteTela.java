package visao;

import java.awt.Dimension;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import modelo.bean.ColeteBean;
import modelo.conexao.Conexao;
import modelo.dao.ColeteDao;
import utilitarios.Datas;

public class ColeteTela extends javax.swing.JInternalFrame {

    Date data;
    java.sql.Date sqlData;
    Conexao con = new Conexao();
    ColeteBean colete = new ColeteBean();
    ColeteDao coleteDao = new ColeteDao();

    public ColeteTela() {
        initComponents();
        listarTodos();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblDescricao = new javax.swing.JLabel();
        jtfDescricao = new javax.swing.JTextField();
        lblMarca = new javax.swing.JLabel();
        jtfTombo = new javax.swing.JTextField();
        lblModelo = new javax.swing.JLabel();
        lblTitulo = new javax.swing.JLabel();
        jcbTipoPesquisa = new javax.swing.JComboBox<>();
        btnExcluir = new javax.swing.JButton();
        jtfPesquisa = new javax.swing.JTextField();
        btnPesquisar = new javax.swing.JButton();
        btnSair = new javax.swing.JButton();
        btnNovo = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtColete = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        btnSalvar = new javax.swing.JButton();
        btnAlterar = new javax.swing.JButton();
        lblCalibre = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jtfQuantidade = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jcTipo = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        jcTamanho = new javax.swing.JComboBox<>();
        jdFabricacao = new com.toedter.calendar.JDateChooser();
        jdValidade = new com.toedter.calendar.JDateChooser();

        lblDescricao.setText("Descrição:");

        jtfDescricao.setEnabled(false);

        lblMarca.setText("Tombo:");

        jtfTombo.setEnabled(false);
        jtfTombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtfTomboActionPerformed(evt);
            }
        });

        lblModelo.setText("Data Fabricação:");

        lblTitulo.setFont(new java.awt.Font("Ubuntu", 1, 24)); // NOI18N
        lblTitulo.setForeground(new java.awt.Color(14, 24, 23));
        lblTitulo.setText("Cadastro de Colete");

        jcbTipoPesquisa.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Descrição", "Tipo" }));
        jcbTipoPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbTipoPesquisaActionPerformed(evt);
            }
        });

        btnExcluir.setText("Excluir");
        btnExcluir.setEnabled(false);
        btnExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirActionPerformed(evt);
            }
        });

        jtfPesquisa.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtfPesquisaMouseClicked(evt);
            }
        });
        jtfPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtfPesquisaActionPerformed(evt);
            }
        });

        btnPesquisar.setText("Pesquisar");
        btnPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPesquisarActionPerformed(evt);
            }
        });

        btnSair.setText("Sair");
        btnSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSairActionPerformed(evt);
            }
        });

        btnNovo.setText("Novo");
        btnNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovoActionPerformed(evt);
            }
        });

        jLabel2.setIcon(new javax.swing.ImageIcon("E:\\Documentos\\NetBeansProjects\\projeto_sagapom\\imagens\\telaprincipal.png")); // NOI18N

        jtColete.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "Descrição", "Tombo", "Tipo", "Tamanho", "Status"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jtColete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtColeteMouseClicked(evt);
            }
        });
        jtColete.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jtColeteKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jtColeteKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jtColeteKeyTyped(evt);
            }
        });
        jScrollPane1.setViewportView(jtColete);

        jLabel1.setIcon(new javax.swing.ImageIcon("E:\\Documentos\\NetBeansProjects\\projeto_sagapom\\imagens\\telaprincipal.png")); // NOI18N

        btnSalvar.setText("Salvar");
        btnSalvar.setEnabled(false);
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        btnAlterar.setText("Alterar");
        btnAlterar.setEnabled(false);
        btnAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAlterarActionPerformed(evt);
            }
        });

        lblCalibre.setText("Data Validade:");

        jLabel3.setText("Quantidade:");

        jtfQuantidade.setEnabled(false);

        jLabel4.setText("Tipo:");

        jcTipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Feminino", "Masculino" }));
        jcTipo.setEnabled(false);

        jLabel5.setText("Tamanho:");

        jcTamanho.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "P", "M", "G", "GG", "EX" }));
        jcTamanho.setEnabled(false);
        jcTamanho.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcTamanhoActionPerformed(evt);
            }
        });

        jdFabricacao.setEnabled(false);

        jdValidade.setEnabled(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 709, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(lblDescricao)
                                        .addComponent(jLabel5))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addGroup(layout.createSequentialGroup()
                                                    .addComponent(jtfDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addGap(20, 20, 20)
                                                    .addComponent(lblMarca)
                                                    .addGap(18, 18, 18)
                                                    .addComponent(jtfTombo, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                    .addComponent(jLabel3)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                    .addComponent(jtfQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGroup(layout.createSequentialGroup()
                                                    .addComponent(jcTamanho, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addGap(46, 46, 46)
                                                    .addComponent(lblModelo)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                    .addComponent(jdFabricacao, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                    .addComponent(lblCalibre)))
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(layout.createSequentialGroup()
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                    .addComponent(jLabel4)
                                                    .addGap(16, 16, 16)
                                                    .addComponent(jcTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                    .addGap(18, 18, 18)
                                                    .addComponent(jdValidade, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                        .addGroup(layout.createSequentialGroup()
                                            .addGap(49, 49, 49)
                                            .addComponent(btnSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(10, 10, 10)
                                            .addComponent(btnAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(26, 26, 26)
                                            .addComponent(btnExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(26, 26, 26)
                                            .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGap(0, 0, Short.MAX_VALUE))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jcbTipoPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(jtfPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(btnPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(264, 264, 264)
                        .addComponent(lblTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 248, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(33, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(lblTitulo))
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jcbTipoPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtfPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 43, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDescricao)
                    .addComponent(jtfDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblMarca)
                    .addComponent(jtfTombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtfQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jcTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5)
                        .addComponent(jcTamanho, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblModelo)
                        .addComponent(lblCalibre))
                    .addComponent(jdFabricacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jdValidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(32, 32, 32)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 9, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jtfTomboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtfTomboActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtfTomboActionPerformed

    private void jcbTipoPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbTipoPesquisaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jcbTipoPesquisaActionPerformed

    private void btnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirActionPerformed
        colete.setCodigo((int) jtColete.getValueAt(jtColete.getSelectedRow(), 0));
        coleteDao.excluirColete(colete);
        listarTodos();
        habilitarCampos(false);
        limparDados();
        habilitarBotoes(true, false, false, false);
    }//GEN-LAST:event_btnExcluirActionPerformed

    private void jtfPesquisaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtfPesquisaMouseClicked
        habilitarCampos(false);
        limparDados();
    }//GEN-LAST:event_jtfPesquisaMouseClicked

    private void jtfPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtfPesquisaActionPerformed

    }//GEN-LAST:event_jtfPesquisaActionPerformed

    private void btnPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPesquisarActionPerformed
        if (jcbTipoPesquisa.getSelectedItem() == "Descrição") {
            listarPorNome(jtfPesquisa.getText());
        } else if (jcbTipoPesquisa.getSelectedItem() == "Tipo") {
            listarPorTipo((jtfPesquisa.getText()));
        }
    }//GEN-LAST:event_btnPesquisarActionPerformed

    private void btnSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSairActionPerformed
        dispose();
    }//GEN-LAST:event_btnSairActionPerformed

    private void btnNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovoActionPerformed
        limparDados();
        habilitarCampos(true);
        habilitarBotoes(true, true, false, false);
    }//GEN-LAST:event_btnNovoActionPerformed

    private void jtColeteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtColeteMouseClicked
        recuperarDados();
    }//GEN-LAST:event_jtColeteMouseClicked

    private void jtColeteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtColeteKeyPressed

    }//GEN-LAST:event_jtColeteKeyPressed

    private void jtColeteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtColeteKeyReleased

    }//GEN-LAST:event_jtColeteKeyReleased

    private void jtColeteKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtColeteKeyTyped

    }//GEN-LAST:event_jtColeteKeyTyped

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        Datas data = new Datas();
        colete.setDescricao(jtfDescricao.getText());
        colete.setTombo(Integer.parseInt(jtfTombo.getText()));
        colete.setQuantidade(Integer.parseInt(jtfQuantidade.getText()));
        colete.setTipo((String) jcTipo.getSelectedItem());
        colete.setTamanho((String) jcTamanho.getSelectedItem());
        try {
            colete.setFabricacao(data.converterDataParaDateUS(jdFabricacao.getDate()));
            colete.setValidade(data.converterDataParaDateUS(jdValidade.getDate()));
        } catch (Exception ex) {
            Logger.getLogger(ColeteTela.class.getName()).log(Level.SEVERE, null, ex);
        }

        coleteDao.cadastrarColete(colete);

        listarTodos();
        limparDados();
        habilitarCampos(false);
        habilitarBotoes(true, false, false, false);

    }//GEN-LAST:event_btnSalvarActionPerformed

    private void btnAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAlterarActionPerformed

        Datas data = new Datas();
        colete.setDescricao(jtfDescricao.getText());
        colete.setTombo(Integer.parseInt(jtfTombo.getText()));
        colete.setQuantidade(Integer.parseInt(jtfQuantidade.getText()));
        colete.setTipo((String) jcTipo.getSelectedItem());
        colete.setTamanho((String) jcTamanho.getSelectedItem());
        colete.setCodigo((int) jtColete.getValueAt(jtColete.getSelectedRow(), 0));

        try {
            colete.setFabricacao(data.converterDataParaDateUS(jdFabricacao.getDate()));
            colete.setValidade(data.converterDataParaDateUS(jdValidade.getDate()));
        } catch (Exception ex) {
            Logger.getLogger(ColeteTela.class.getName()).log(Level.SEVERE, null, ex);
        }

        coleteDao.alterarColete(colete);

        listarTodos();
        limparDados();
        habilitarCampos(false);
        habilitarBotoes(true, false, false, false);
    }//GEN-LAST:event_btnAlterarActionPerformed

    private void jcTamanhoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcTamanhoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jcTamanhoActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAlterar;
    private javax.swing.JButton btnExcluir;
    private javax.swing.JButton btnNovo;
    private javax.swing.JButton btnPesquisar;
    private javax.swing.JButton btnSair;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JComboBox<String> jcTamanho;
    private javax.swing.JComboBox<String> jcTipo;
    private javax.swing.JComboBox<String> jcbTipoPesquisa;
    private com.toedter.calendar.JDateChooser jdFabricacao;
    private com.toedter.calendar.JDateChooser jdValidade;
    private javax.swing.JTable jtColete;
    private javax.swing.JTextField jtfDescricao;
    private javax.swing.JTextField jtfPesquisa;
    private javax.swing.JTextField jtfQuantidade;
    private javax.swing.JTextField jtfTombo;
    private javax.swing.JLabel lblCalibre;
    private javax.swing.JLabel lblDescricao;
    private javax.swing.JLabel lblMarca;
    private javax.swing.JLabel lblModelo;
    private javax.swing.JLabel lblTitulo;
    // End of variables declaration//GEN-END:variables
    public void setPosicao() {
        Dimension d = this.getDesktopPane().getSize();
        this.setLocation((d.width - this.getSize().width) / 2, (d.height - this.getSize().height) / 2);
    }

    public void habilitarBotoes(boolean N, boolean S, boolean C, boolean D) {
        btnNovo.setEnabled(N);
        btnSalvar.setEnabled(S);
        btnAlterar.setEnabled(C);
        btnExcluir.setEnabled(D);
    }

    public void habilitarCampos(boolean valor) {
        jtfDescricao.setEnabled(valor);
        jtfTombo.setEnabled(valor);
        jtfQuantidade.setEnabled(valor);
        jcTipo.setEnabled(valor);
        jcTamanho.setEnabled(valor);
        jdFabricacao.setEnabled(valor);
        jdValidade.setEnabled(valor);
    }

    public void limparDados() {
        jtfDescricao.setText("");
        jtfTombo.setText("");
        jtfQuantidade.setText("");
        jcTipo.setSelectedIndex(0);
        jcTamanho.setSelectedIndex(0);
        jdFabricacao.setDate(null);
        jdValidade.setDate(null);
    }

    public void listarTodos() {
        DefaultTableModel dtmColetes = (DefaultTableModel) jtColete.getModel();
        dtmColetes.setNumRows(0);

        coleteDao.listarTodos().forEach((colete) -> {
            dtmColetes.addRow(new Object[]{
                colete.getCodigo(),
                colete.getDescricao(),
                colete.getTombo(),
                colete.getTipo(),
                colete.getTamanho(),
                colete.getStatus() == 1 ? "Disponivel" : "Não Disponível"
            });
        });
    }

    public void listarPorNome(String nome) {
        DefaultTableModel dtmColetes = (DefaultTableModel) jtColete.getModel();
        dtmColetes.setNumRows(0);

        coleteDao.listarPorNome(nome).forEach((colete) -> {
            dtmColetes.addRow(new Object[]{
                colete.getCodigo(),
                colete.getDescricao(),
                colete.getTombo(),
                colete.getTipo(),
                colete.getTamanho(),
                colete.getStatus() == 1 ? "Disponivel" : "Não Disponível"
            });
        });
    }

    public void listarPorTipo(String tipo) {
        DefaultTableModel dtmColetes = (DefaultTableModel) jtColete.getModel();
        dtmColetes.setNumRows(0);

        coleteDao.listarPorTipo(tipo).forEach((colete) -> {
            dtmColetes.addRow(new Object[]{
                colete.getCodigo(),
                colete.getDescricao(),
                colete.getTombo(),
                colete.getTipo(),
                colete.getTamanho(),
                colete.getStatus() == 1 ? "Disponivel" : "Não Disponível"
            });
        });
    }

    public void recuperarDados() {
        PreparedStatement ps;
        ResultSet rs;
        Connection con;
        String sql;

        try {
            con = Conexao.pegarConexao();
            int row = jtColete.getSelectedRow();
            String table_click = (jtColete.getModel().getValueAt(row, 0).toString());
            sql = "select equ_descricao, equ_qtd, col_tombo, col_tamanho, col_tipo, col_fabricacao, col_validade "
                    + " from equipamento"
                    + " inner join  colete on col_equ_cod = equ_cod"
                    + " where col_equ_cod='" + table_click + "' ";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                //Dados Básicos
                String add1 = rs.getString("equ_descricao");
                jtfDescricao.setText(add1);

                String add2 = rs.getString("col_tombo");
                jtfTombo.setText(add2);

                String add3 = rs.getString("equ_qtd");
                jtfQuantidade.setText(add3);

                String add4 = rs.getString("col_tipo");
                jcTipo.setSelectedItem(add4);

                String add5 = rs.getString("col_tamanho");
                jcTamanho.setSelectedItem(add5);

                Date add6 = rs.getDate("col_fabricacao");
                jdFabricacao.setDate(add6);

                Date add7 = rs.getDate("col_validade");
                jdValidade.setDate(add7);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
        habilitarCampos(true);
        habilitarBotoes(true, false, true, true);
    }
}
