package visao;

import java.awt.Dimension;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.bean.MunicaoBean;
import modelo.conexao.Conexao;
import modelo.dao.MunicaoDao;

public class MunicaoTela extends javax.swing.JInternalFrame {

    MunicaoBean municao = new MunicaoBean();
    MunicaoDao municaoDao = new MunicaoDao();

    public MunicaoTela() {
        initComponents();
        listarTodas();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnExcluir = new javax.swing.JButton();
        jtfQuantidade = new javax.swing.JTextField();
        btnSair = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        lblDescricao = new javax.swing.JLabel();
        jtfDescricao = new javax.swing.JTextField();
        lblMarca = new javax.swing.JLabel();
        jtfMarca = new javax.swing.JTextField();
        lblModelo = new javax.swing.JLabel();
        lblTitulo = new javax.swing.JLabel();
        jcbTipoPesquisa = new javax.swing.JComboBox<>();
        jtfPesquisa = new javax.swing.JTextField();
        btnPesquisar = new javax.swing.JButton();
        btnNovo = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtMunicao = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        btnSalvar = new javax.swing.JButton();
        jtfCalibre = new javax.swing.JTextField();
        btnAlterar = new javax.swing.JButton();
        lblCalibre = new javax.swing.JLabel();

        btnExcluir.setText("Excluir");
        btnExcluir.setEnabled(false);
        btnExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirActionPerformed(evt);
            }
        });

        jtfQuantidade.setEnabled(false);

        btnSair.setText("Sair");
        btnSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSairActionPerformed(evt);
            }
        });

        jLabel2.setIcon(new javax.swing.ImageIcon("E:\\Documentos\\NetBeansProjects\\projeto_sagapom\\imagens\\telaprincipal.png")); // NOI18N

        lblDescricao.setText("Descrição:");

        jtfDescricao.setEnabled(false);

        lblMarca.setText("Marca:");

        jtfMarca.setEnabled(false);
        jtfMarca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtfMarcaActionPerformed(evt);
            }
        });

        lblModelo.setText("Calibre:");

        lblTitulo.setFont(new java.awt.Font("Ubuntu", 1, 24)); // NOI18N
        lblTitulo.setForeground(new java.awt.Color(14, 24, 23));
        lblTitulo.setText("Cadastro de Munição");

        jcbTipoPesquisa.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Nome", "Calibre" }));
        jcbTipoPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbTipoPesquisaActionPerformed(evt);
            }
        });

        jtfPesquisa.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtfPesquisaMouseClicked(evt);
            }
        });
        jtfPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtfPesquisaActionPerformed(evt);
            }
        });

        btnPesquisar.setText("Pesquisar");
        btnPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPesquisarActionPerformed(evt);
            }
        });

        btnNovo.setText("Novo");
        btnNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovoActionPerformed(evt);
            }
        });

        jtMunicao.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "Nome", "Marca", "Calibre", "Status"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jtMunicao.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtMunicaoMouseClicked(evt);
            }
        });
        jtMunicao.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jtMunicaoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jtMunicaoKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jtMunicaoKeyTyped(evt);
            }
        });
        jScrollPane1.setViewportView(jtMunicao);

        jLabel1.setIcon(new javax.swing.ImageIcon("E:\\Documentos\\NetBeansProjects\\projeto_sagapom\\imagens\\telaprincipal.png")); // NOI18N

        btnSalvar.setText("Salvar");
        btnSalvar.setEnabled(false);
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        jtfCalibre.setEnabled(false);

        btnAlterar.setText("Alterar");
        btnAlterar.setEnabled(false);
        btnAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAlterarActionPerformed(evt);
            }
        });

        lblCalibre.setText("Quantidade:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblDescricao)
                            .addComponent(lblModelo))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(btnAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(btnExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jtfCalibre, javax.swing.GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE)
                                    .addComponent(jtfDescricao))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(lblMarca)
                                        .addGap(18, 18, 18)
                                        .addComponent(jtfMarca))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(lblCalibre)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jtfQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jcbTipoPesquisa, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jtfPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(79, 79, 79)
                                .addComponent(btnNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 593, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 662, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(229, 229, 229)
                .addComponent(lblTitulo)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(lblTitulo))
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jcbTipoPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtfPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnNovo, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDescricao)
                    .addComponent(jtfDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblMarca)
                    .addComponent(jtfMarca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtfQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblCalibre)
                    .addComponent(jtfCalibre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblModelo))
                .addGap(34, 34, 34)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAlterar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnExcluir, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 9, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirActionPerformed
        municao.setCodigo((int) jtMunicao.getValueAt(jtMunicao.getSelectedRow(), 0));
        municaoDao.excluirMunicao(municao);
        listarTodas();
    }//GEN-LAST:event_btnExcluirActionPerformed

    private void btnSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSairActionPerformed
        dispose();
    }//GEN-LAST:event_btnSairActionPerformed

    private void jtfMarcaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtfMarcaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtfMarcaActionPerformed

    private void jcbTipoPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbTipoPesquisaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jcbTipoPesquisaActionPerformed

    private void jtfPesquisaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtfPesquisaMouseClicked
        //   habilitarCampos(false);
        //  limparDados();
    }//GEN-LAST:event_jtfPesquisaMouseClicked

    private void jtfPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtfPesquisaActionPerformed

    }//GEN-LAST:event_jtfPesquisaActionPerformed

    private void btnPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPesquisarActionPerformed
        if (jcbTipoPesquisa.getSelectedItem() == "Nome") {
            listarPorNome(jtfPesquisa.getText());
        } else if (jcbTipoPesquisa.getSelectedItem() == "Calibre") {
            listarPorCalibre(jtfPesquisa.getText());
        }
    }//GEN-LAST:event_btnPesquisarActionPerformed

    private void btnNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovoActionPerformed
        limparDados();
        habilitarCampos(true);
        habilitarBotoes(true, true, false, false);
    }//GEN-LAST:event_btnNovoActionPerformed

    private void jtMunicaoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtMunicaoMouseClicked
        recuperarDados();
    }//GEN-LAST:event_jtMunicaoMouseClicked

    private void jtMunicaoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtMunicaoKeyPressed

    }//GEN-LAST:event_jtMunicaoKeyPressed

    private void jtMunicaoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtMunicaoKeyReleased

    }//GEN-LAST:event_jtMunicaoKeyReleased

    private void jtMunicaoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtMunicaoKeyTyped

    }//GEN-LAST:event_jtMunicaoKeyTyped

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        municao.setDescricao(jtfDescricao.getText());
        municao.setMarca(jtfMarca.getText());
        municao.setCalibre(jtfCalibre.getText());
        municao.setQuantidade(Integer.parseInt(jtfQuantidade.getText()));

        municaoDao.salvarMunicao(municao);
        listarTodas();
        limparDados();
        habilitarCampos(false);
        habilitarBotoes(true, false, false, false);
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void btnAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAlterarActionPerformed
        municao.setDescricao(jtfDescricao.getText());
        municao.setMarca(jtfMarca.getText());
        municao.setCalibre(jtfCalibre.getText());
        municao.setQuantidade(Integer.parseInt(jtfQuantidade.getText()));
        municao.setCodigo((int) jtMunicao.getValueAt(jtMunicao.getSelectedRow(), 0));

        municaoDao.alterarMunicao(municao);
        listarTodas();
        limparDados();
        habilitarCampos(false);
        habilitarBotoes(true, false, false, false);
    }//GEN-LAST:event_btnAlterarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAlterar;
    private javax.swing.JButton btnExcluir;
    private javax.swing.JButton btnNovo;
    private javax.swing.JButton btnPesquisar;
    private javax.swing.JButton btnSair;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JComboBox<String> jcbTipoPesquisa;
    private javax.swing.JTable jtMunicao;
    private javax.swing.JTextField jtfCalibre;
    private javax.swing.JTextField jtfDescricao;
    private javax.swing.JTextField jtfMarca;
    private javax.swing.JTextField jtfPesquisa;
    private javax.swing.JTextField jtfQuantidade;
    private javax.swing.JLabel lblCalibre;
    private javax.swing.JLabel lblDescricao;
    private javax.swing.JLabel lblMarca;
    private javax.swing.JLabel lblModelo;
    private javax.swing.JLabel lblTitulo;
    // End of variables declaration//GEN-END:variables

    public void setPosicao() {
        Dimension d = this.getDesktopPane().getSize();
        this.setLocation((d.width - this.getSize().width) / 2, (d.height - this.getSize().height) / 2);
    }

    public void habilitarBotoes(boolean N, boolean S, boolean C, boolean D) {
        btnNovo.setEnabled(N);
        btnSalvar.setEnabled(S);
        btnAlterar.setEnabled(C);
        btnExcluir.setEnabled(D);
    }

    public void habilitarCampos(boolean valor) {
        jtfDescricao.setEnabled(valor);
        jtfMarca.setEnabled(valor);
        jtfCalibre.setEnabled(valor);
        jtfQuantidade.setEnabled(valor);
    }

    public void limparDados() {
        jtfDescricao.setText("");
        jtfMarca.setText("");
        jtfCalibre.setText("");
        jtfQuantidade.setText("");
    }

    public void listarTodas() {
        DefaultTableModel dtmMunicoes = (DefaultTableModel) jtMunicao.getModel();
        dtmMunicoes.setNumRows(0);

        municaoDao.listarTodas().forEach((armeiro) -> {
            dtmMunicoes.addRow(new Object[]{
                armeiro.getCodigo(),
                armeiro.getDescricao(),
                armeiro.getMarca(),
                armeiro.getCalibre(),
                armeiro.getStatus() == 1 ? "Disponivel" : "Não Disponível"
            });
        });
    }

    public void listarPorNome(String nome) {
        DefaultTableModel dtmMunicoes = (DefaultTableModel) jtMunicao.getModel();
        dtmMunicoes.setNumRows(0);

        municaoDao.listarPorNome(nome).forEach((municao) -> {
            dtmMunicoes.addRow(new Object[]{
                municao.getCodigo(),
                municao.getDescricao(),
                municao.getMarca(),
                municao.getCalibre(),
                municao.getStatus() == 1 ? "Disponivel" : "Não Disponível"
            });
        });
    }

    public void listarPorCalibre(String calibre) {
        DefaultTableModel dtmMunicoes = (DefaultTableModel) jtMunicao.getModel();
        dtmMunicoes.setNumRows(0);

        municaoDao.listarPorCalibre(calibre).forEach((municao) -> {
            dtmMunicoes.addRow(new Object[]{
                municao.getCodigo(),
                municao.getDescricao(),
                municao.getMarca(),
                municao.getCalibre(),
                municao.getStatus() == 1 ? "Disponivel" : "Não Disponível"
            });
        });
    }

    public void recuperarDados() {
        PreparedStatement ps;
        ResultSet rs;
        Connection con;
        String sql;

        try {
            con = Conexao.pegarConexao();
            int row = jtMunicao.getSelectedRow();
            String table_click = (jtMunicao.getModel().getValueAt(row, 0).toString());
            sql = "select equ_descricao, equ_qtd, mun_marca, mun_calibre"
                    + " from equipamento"
                    + " inner join  municao on mun_equ_cod = equ_cod"
                    + " where mun_equ_cod='" + table_click + "' ";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                //Dados Básicos
                String add1 = rs.getString("equ_descricao");
                jtfDescricao.setText(add1);

                String add2 = rs.getString("mun_marca");
                jtfMarca.setText(add2);

                String add5 = rs.getString("mun_calibre");
                jtfCalibre.setText(add5);

                String add6 = rs.getString("equ_qtd");
                jtfQuantidade.setText(add6);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
        habilitarCampos(true);
        habilitarBotoes(true, false, true, true);
    }

}
