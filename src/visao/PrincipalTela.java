package visao;

import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.JFrame;
import utilitarios.FundoTela;

public class PrincipalTela extends javax.swing.JFrame {

    FundoTela telaFundo;
    ArmeiroTela armeiroTela;
    PolicialTela policialTela;
    ArmaTela armaTela;
    MunicaoTela municaoTela;
    ColeteTela coleteTela;

    public PrincipalTela() {
        initComponents();
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        getContentPane().setBackground(Color.WHITE);
        
 //       JDesktopPane desktop;
//desktop = new JDesktopPane(){
//         Image im = (new ImageIcon("background.jpg")).getImage(); 
//         public void paintComponent(Graphics g){        
//          g.drawImage(im,0,0,this);            }
  //                                   };//fim do JDesktopPane
        
    }
    
    

     @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDesktopPane1 = new javax.swing.JDesktopPane();
        jLabel1 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jmCadastro = new javax.swing.JMenu();
        menuUsuario = new javax.swing.JMenuItem();
        menuPolicial = new javax.swing.JMenuItem();
        menuArmamento = new javax.swing.JMenu();
        submenuArma = new javax.swing.JMenuItem();
        subMenuMunicao = new javax.swing.JMenuItem();
        subMenuColete = new javax.swing.JMenuItem();
        jmLocacao = new javax.swing.JMenu();
        jmRelatorio = new javax.swing.JMenu();
        jmSobre = new javax.swing.JMenu();
        jmSair = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("SISARM - Sistema Administrativo para Reserva de Armamento");
        getContentPane().setLayout(null);

        jDesktopPane1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setIcon(new javax.swing.ImageIcon("E:\\Documentos\\NetBeansProjects\\projeto_sagapom\\imagens\\sisarm3.png")); // NOI18N
        jLabel1.setDoubleBuffered(true);
        jLabel1.setRequestFocusEnabled(false);
        jLabel1.setVerifyInputWhenFocusTarget(false);
        jDesktopPane1.add(jLabel1);
        jLabel1.setBounds(220, 180, 970, 340);
        jLabel1.getAccessibleContext().setAccessibleParent(this);

        getContentPane().add(jDesktopPane1);
        jDesktopPane1.setBounds(-40, 0, 1440, 700);

        jMenuBar1.setBackground(new java.awt.Color(123, 207, 167));
        jMenuBar1.setForeground(new java.awt.Color(123, 207, 167));

        jmCadastro.setBackground(new java.awt.Color(255, 255, 255));
        jmCadastro.setForeground(new java.awt.Color(255, 255, 255));
        jmCadastro.setIcon(new javax.swing.ImageIcon("E:\\Documentos\\NetBeansProjects\\projeto_sagapom\\imagens\\menu_cadastro.png")); // NOI18N
        jmCadastro.setText("Cadastro");

        menuUsuario.setText("Policial");
        menuUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUsuarioActionPerformed(evt);
            }
        });
        jmCadastro.add(menuUsuario);

        menuPolicial.setText("Armeiro");
        menuPolicial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPolicialActionPerformed(evt);
            }
        });
        jmCadastro.add(menuPolicial);

        menuArmamento.setText("Equipamento");

        submenuArma.setText("Armas");
        submenuArma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submenuArmaActionPerformed(evt);
            }
        });
        menuArmamento.add(submenuArma);

        subMenuMunicao.setText("Munição");
        subMenuMunicao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                subMenuMunicaoActionPerformed(evt);
            }
        });
        menuArmamento.add(subMenuMunicao);

        subMenuColete.setText("Colete");
        subMenuColete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                subMenuColeteActionPerformed(evt);
            }
        });
        menuArmamento.add(subMenuColete);

        jmCadastro.add(menuArmamento);

        jMenuBar1.add(jmCadastro);

        jmLocacao.setForeground(new java.awt.Color(255, 255, 255));
        jmLocacao.setIcon(new javax.swing.ImageIcon("E:\\Documentos\\NetBeansProjects\\projeto_sagapom\\imagens\\locaco3.png")); // NOI18N
        jmLocacao.setText("Locação");
        jMenuBar1.add(jmLocacao);

        jmRelatorio.setForeground(new java.awt.Color(255, 255, 255));
        jmRelatorio.setIcon(new javax.swing.ImageIcon("E:\\Documentos\\NetBeansProjects\\projeto_sagapom\\imagens\\relatorio.png")); // NOI18N
        jmRelatorio.setText("Relatórios");
        jMenuBar1.add(jmRelatorio);

        jmSobre.setForeground(new java.awt.Color(255, 255, 255));
        jmSobre.setIcon(new javax.swing.ImageIcon("E:\\Documentos\\NetBeansProjects\\projeto_sagapom\\imagens\\sobre2.png")); // NOI18N
        jmSobre.setText("Sobre");
        jMenuBar1.add(jmSobre);

        jmSair.setForeground(new java.awt.Color(255, 255, 255));
        jmSair.setIcon(new javax.swing.ImageIcon("E:\\Documentos\\NetBeansProjects\\projeto_sagapom\\imagens\\sair.png")); // NOI18N
        jmSair.setText("Sair");
        jMenuBar1.add(jmSair);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void menuPolicialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPolicialActionPerformed
        armeiroTela = new ArmeiroTela();
        jDesktopPane1.add(armeiroTela);
        armeiroTela.hasFocus();
        armeiroTela.setVisible(true);
    }//GEN-LAST:event_menuPolicialActionPerformed

    private void menuUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUsuarioActionPerformed
        policialTela = new PolicialTela();
        policialTela.setVisible(true);
    }//GEN-LAST:event_menuUsuarioActionPerformed

    private void subMenuMunicaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_subMenuMunicaoActionPerformed
        municaoTela = new MunicaoTela();
        municaoTela.setVisible(true);
    }//GEN-LAST:event_subMenuMunicaoActionPerformed

    private void submenuArmaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submenuArmaActionPerformed
        armaTela = new ArmaTela();
        armaTela.setVisible(true);
    }//GEN-LAST:event_submenuArmaActionPerformed

    private void subMenuColeteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_subMenuColeteActionPerformed
        coleteTela = new ColeteTela();
        coleteTela.setVisible(true);
    }//GEN-LAST:event_subMenuColeteActionPerformed

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            new PrincipalTela().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenu jmCadastro;
    private javax.swing.JMenu jmLocacao;
    private javax.swing.JMenu jmRelatorio;
    private javax.swing.JMenu jmSair;
    private javax.swing.JMenu jmSobre;
    private javax.swing.JMenu menuArmamento;
    private javax.swing.JMenuItem menuPolicial;
    private javax.swing.JMenuItem menuUsuario;
    private javax.swing.JMenuItem subMenuColete;
    private javax.swing.JMenuItem subMenuMunicao;
    private javax.swing.JMenuItem submenuArma;
    // End of variables declaration//GEN-END:variables
}
