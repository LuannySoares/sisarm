package visao;

import controle.PolicialControle;
import java.awt.Dimension;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.MaskFormatter;
import modelo.bean.EnderecoBean;
import modelo.bean.FuncaoBean;
import modelo.bean.GraduacaoBean;
import modelo.bean.PolicialBean;
import modelo.conexao.Conexao;
import modelo.dao.FuncaoDao;
import modelo.dao.GraduacaoDao;
import modelo.dao.PolicialDao;
import utilitarios.WebServiceCep;

public class PolicialTela extends javax.swing.JInternalFrame {

    MaskFormatter formatoTelefone;
    MaskFormatter formatoCep;

    Conexao con = new Conexao();

    PolicialBean policial = new PolicialBean();
    PolicialDao policialDao = new PolicialDao();
    PolicialControle policialC = new PolicialControle();

    EnderecoBean endereco = new EnderecoBean();

    public PolicialTela() {
        initComponents();
        preencherComboGraduacao(jcGraduacao);
        preencherComboFuncao(jcFuncao);
        habilitarCampos(false);
        listarTodos();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblTitulo = new javax.swing.JLabel();
        jcbTipoPesquisa = new javax.swing.JComboBox<>();
        jtfPesquisa = new javax.swing.JTextField();
        btnPesquisar = new javax.swing.JButton();
        btnNovo = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtPoliciais = new javax.swing.JTable();
        btnSalvar = new javax.swing.JButton();
        btnAlterar = new javax.swing.JButton();
        btnDesativar = new javax.swing.JButton();
        btnSair = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jtpDados = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        lblNome = new javax.swing.JLabel();
        jtfNome = new javax.swing.JTextField();
        lblRg = new javax.swing.JLabel();
        jtfRg = new javax.swing.JTextField();
        lblNomeGuerra = new javax.swing.JLabel();
        jtfNomeGuerra = new javax.swing.JTextField();
        lblPotoGraduacao = new javax.swing.JLabel();
        lblTelefone = new javax.swing.JLabel();
        try {
            formatoTelefone = new MaskFormatter("(##) #####-####");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao inserir campo personalizado");
        }
        jtfTelefone = new JFormattedTextField(formatoTelefone);

        ;
        btnProximo1 = new javax.swing.JToggleButton();
        jcGraduacao = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        jcFuncao = new javax.swing.JComboBox();
        jPanel2 = new javax.swing.JPanel();
        lblCep = new javax.swing.JLabel();
        lblBairro = new javax.swing.JLabel();
        jtfEndereco = new javax.swing.JTextField();
        lblEndereço = new javax.swing.JLabel();
        lblEstado = new javax.swing.JLabel();
        lblNum = new javax.swing.JLabel();
        jtfNum = new javax.swing.JTextField();
        lblCidade = new javax.swing.JLabel();
        try {
            formatoTelefone = new MaskFormatter("#####-###");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao inserir campo personalizado");
        }
        jtfCep = new JFormattedTextField(formatoTelefone); ;
        jtfCidade = new javax.swing.JTextField();
        jcEstado = new javax.swing.JComboBox();
        jbBuscar = new javax.swing.JButton();
        jtfBairro = new javax.swing.JTextField();
        btnAnterior1 = new javax.swing.JButton();

        getContentPane().setLayout(null);

        lblTitulo.setFont(new java.awt.Font("Ubuntu", 1, 24)); // NOI18N
        lblTitulo.setForeground(new java.awt.Color(14, 24, 23));
        lblTitulo.setText("Cadastro de Policial");
        getContentPane().add(lblTitulo);
        lblTitulo.setBounds(336, 11, 230, 32);

        jcbTipoPesquisa.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Nome de Guerra", "RG" }));
        jcbTipoPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcbTipoPesquisaActionPerformed(evt);
            }
        });
        getContentPane().add(jcbTipoPesquisa);
        jcbTipoPesquisa.setBounds(30, 88, 150, 20);

        jtfPesquisa.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtfPesquisaMouseClicked(evt);
            }
        });
        jtfPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtfPesquisaActionPerformed(evt);
            }
        });
        getContentPane().add(jtfPesquisa);
        jtfPesquisa.setBounds(200, 88, 221, 20);

        btnPesquisar.setText("Pesquisar");
        btnPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPesquisarActionPerformed(evt);
            }
        });
        getContentPane().add(btnPesquisar);
        btnPesquisar.setBounds(430, 88, 126, 25);

        btnNovo.setText("Novo");
        btnNovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNovoActionPerformed(evt);
            }
        });
        getContentPane().add(btnNovo);
        btnNovo.setBounds(780, 88, 110, 25);

        jtPoliciais.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "Nome ", "Nome de Guerra", "RG", "Telefone", "Posto/Graduação", "Status"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, true, true, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jtPoliciais.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtPoliciaisMouseClicked(evt);
            }
        });
        jtPoliciais.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jtPoliciaisKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jtPoliciaisKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jtPoliciaisKeyTyped(evt);
            }
        });
        jScrollPane1.setViewportView(jtPoliciais);

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(50, 143, 860, 200);

        btnSalvar.setText("Salvar");
        btnSalvar.setEnabled(false);
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });
        getContentPane().add(btnSalvar);
        btnSalvar.setBounds(260, 558, 110, 25);

        btnAlterar.setText("Alterar");
        btnAlterar.setEnabled(false);
        btnAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAlterarActionPerformed(evt);
            }
        });
        getContentPane().add(btnAlterar);
        btnAlterar.setBounds(380, 558, 100, 25);

        btnDesativar.setText("Desativar");
        btnDesativar.setEnabled(false);
        btnDesativar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDesativarActionPerformed(evt);
            }
        });
        getContentPane().add(btnDesativar);
        btnDesativar.setBounds(490, 558, 110, 25);

        btnSair.setText("Sair");
        btnSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSairActionPerformed(evt);
            }
        });
        getContentPane().add(btnSair);
        btnSair.setBounds(610, 558, 110, 25);

        jLabel1.setIcon(new javax.swing.ImageIcon("E:\\Documentos\\NetBeansProjects\\projeto_sagapom\\imagens\\telaprincipal.png")); // NOI18N
        getContentPane().add(jLabel1);
        jLabel1.setBounds(0, 8, 920, 60);

        jLabel2.setIcon(new javax.swing.ImageIcon("E:\\Documentos\\NetBeansProjects\\projeto_sagapom\\imagens\\telaprincipal.png")); // NOI18N
        getContentPane().add(jLabel2);
        jLabel2.setBounds(0, 608, 920, 10);

        jtpDados.setEnabled(false);

        lblNome.setText("Nome: ");

        jtfNome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtfNomeActionPerformed(evt);
            }
        });

        lblRg.setText("Rg: ");

        lblNomeGuerra.setText("Nome de Guerra: ");

        jtfNomeGuerra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtfNomeGuerraActionPerformed(evt);
            }
        });

        lblPotoGraduacao.setText("Posto/Graduação:");

        lblTelefone.setText("Telefone:");

        btnProximo1.setText("Próximo");
        btnProximo1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProximo1ActionPerformed(evt);
            }
        });

        jcGraduacao.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selecionar Item" }));
        jcGraduacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcGraduacaoActionPerformed(evt);
            }
        });

        jLabel3.setText("Função:");

        jcFuncao.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selecionar Item" }));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(11, 675, Short.MAX_VALUE)
                        .addComponent(btnProximo1, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(23, 23, 23))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(11, 11, 11)
                                .addComponent(lblNome)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jtfNome, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jcFuncao, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(26, 26, 26)
                                .addComponent(lblTelefone)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jtfTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblRg)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jtfRg, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(lblNomeGuerra)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jtfNomeGuerra, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(lblPotoGraduacao)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jcGraduacao, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGap(10, 10, 10)))
                .addGap(34, 34, 34))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(73, 73, 73)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3)
                        .addComponent(jcFuncao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblTelefone)
                        .addComponent(jtfTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblRg)
                        .addComponent(jtfRg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 15, Short.MAX_VALUE)
                .addComponent(btnProximo1)
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblNome)
                    .addComponent(jtfNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNomeGuerra)
                    .addComponent(jtfNomeGuerra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblPotoGraduacao)
                    .addComponent(jcGraduacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jtpDados.addTab("Dados Básicos ", jPanel1);

        lblCep.setText("Cep:");

        lblBairro.setText("Bairro:");

        jtfEndereco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtfEnderecoActionPerformed(evt);
            }
        });

        lblEndereço.setText("Logradouro:");

        lblEstado.setText("Estado:");

        lblNum.setText("Nº:");

        lblCidade.setText("Cidade:");

        jtfCep.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtfCepActionPerformed(evt);
            }
        });

        jtfCidade.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtfCidadeActionPerformed(evt);
            }
        });

        jcEstado.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "AC", "AL", "AM", "AP", "BA", "CE", "DF", "ES", "GO", "MA", "MG", "MS", "MT", "PA", "PB", "PE", "PI", "PR", "RJ", "RN", "RO", "RR", "RS", "SC", "SE", "SP", "TO" }));

        jbBuscar.setText("Buscar");
        jbBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbBuscarActionPerformed(evt);
            }
        });

        btnAnterior1.setText("Anterior");
        btnAnterior1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnterior1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(lblBairro)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jtfBairro, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(lblCep)
                        .addGap(18, 18, 18)
                        .addComponent(jtfCep, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jbBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 51, Short.MAX_VALUE)
                        .addComponent(lblEndereço)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jtfEndereco, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26)
                        .addComponent(lblNum)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jtfNum, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(18, 18, Short.MAX_VALUE)
                        .addComponent(lblEstado)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jcEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblCidade)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jtfCidade, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnAnterior1, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(46, 46, 46))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCep)
                    .addComponent(lblEndereço)
                    .addComponent(jtfEndereco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtfNum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNum)
                    .addComponent(jtfCep, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jbBuscar))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblBairro)
                    .addComponent(lblEstado)
                    .addComponent(lblCidade)
                    .addComponent(jtfCidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jcEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtfBairro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(btnAnterior1)
                .addContainerGap(19, Short.MAX_VALUE))
        );

        jtpDados.addTab("Endereço", jPanel2);

        getContentPane().add(jtpDados);
        jtpDados.setBounds(40, 358, 850, 170);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jcbTipoPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcbTipoPesquisaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jcbTipoPesquisaActionPerformed

    private void jtfPesquisaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtfPesquisaMouseClicked
        habilitarCampos(false);
        limparDados();
    }//GEN-LAST:event_jtfPesquisaMouseClicked

    private void jtfPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtfPesquisaActionPerformed

    }//GEN-LAST:event_jtfPesquisaActionPerformed

    private void btnPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPesquisarActionPerformed
        if (jcbTipoPesquisa.getSelectedItem() == "Nome de Guerra") {
            PesqNomeGuerra(jtfPesquisa.getText());
        } else if (jcbTipoPesquisa.getSelectedItem() == "RG") {
            PesqRG(jtfPesquisa.getText());
        }
    }//GEN-LAST:event_btnPesquisarActionPerformed

    private void btnNovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNovoActionPerformed
        limparDados();
        habilitarCampos(true);
        habilitarBotoes(true, true, false, false);
    }//GEN-LAST:event_btnNovoActionPerformed

    private void jtPoliciaisMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtPoliciaisMouseClicked
        recuperarDados();
    }//GEN-LAST:event_jtPoliciaisMouseClicked

    private void jtPoliciaisKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtPoliciaisKeyPressed

    }//GEN-LAST:event_jtPoliciaisKeyPressed

    private void jtPoliciaisKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtPoliciaisKeyReleased
        recuperarDados();
    }//GEN-LAST:event_jtPoliciaisKeyReleased

    private void jtPoliciaisKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtPoliciaisKeyTyped

    }//GEN-LAST:event_jtPoliciaisKeyTyped

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        if (jtfNome.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Preencha o campo 'Nome' para continuar!");
            jtfNome.requestFocus();
        } else if (jtfNomeGuerra.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Preencha o campo 'Nome de Guerra' para continuar!");
            jtfNomeGuerra.requestFocus();
        } else if (jcGraduacao.getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(null, "Escolha um posto ou uma graduação!");
            jcGraduacao.requestFocus();
        } else if (jcFuncao.getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(null, "Escolha uma função para continuar!");
            jcFuncao.requestFocus();
        } else if (jtfTelefone.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Preencha o campo 'Telefone' para continuar!");
            jtfTelefone.requestFocus();
        } else if (jtfRg.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Preencha o campo 'RG' para continuar!");
            jtfRg.requestFocus();
        } else if (jtfCep.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Preencha o campo 'CEP' para continuar!");
            jtfCep.requestFocus();
        } else if (jtfEndereco.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Preencha o campo 'Logradouro' para continuar!");
            jtfEndereco.requestFocus();
        } else if (jtfNum.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Preencha o campo 'Número' para continuar!");
            jtfNum.requestFocus();
        } else if (jtfBairro.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Preencha o campo 'Bairro' para continuar!");
            jtfBairro.requestFocus();
        } else if (jtfCidade.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Preencha o campo 'Cidade' para continuar!");
            jtfRg.requestFocus();
        } else {

            //Dados Básicos
            policial.setNome(jtfNome.getText());
            policial.setNome_guerra(jtfNomeGuerra.getText());
            policial.setRg(jtfRg.getText());
            policial.setTelefone(jtfTelefone.getText());

            GraduacaoBean g = (GraduacaoBean) jcGraduacao.getSelectedItem();
            policial.setGraduacaoBean(g);

            FuncaoBean f = (FuncaoBean) jcFuncao.getSelectedItem();
            policial.setFuncaoBean(f);

            //Dados do Endereco
            endereco.setCep(jtfCep.getText());
            endereco.setDescricao(jtfEndereco.getText());
            endereco.setBairro(jtfBairro.getText());
            endereco.setNum(jtfNum.getText());
            endereco.setEstado((String) jcEstado.getSelectedItem());
            endereco.setCidade(jtfCidade.getText());

            //Método Salvar
            policialC.salvar(endereco, policial);

            //Ações
            listarTodos();
            limparDados();
            habilitarCampos(false);
            habilitarBotoes(true, false, false, false);
            jtpDados.setSelectedIndex(0);
        }
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void btnAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAlterarActionPerformed
        if (jtfNome.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Preencha o campo 'Nome' para continuar!");
            jtfNome.requestFocus();
        } else if (jtfNomeGuerra.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Preencha o campo 'Nome de Guerra' para continuar!");
            jtfNomeGuerra.requestFocus();
        } else if (jcGraduacao.getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(null, "Escolha um posto ou uma graduação!");
            jcGraduacao.requestFocus();
        } else if (jcFuncao.getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(null, "Escolha uma função para continuar!");
            jcFuncao.requestFocus();
        } else if (jtfTelefone.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Preencha o campo 'Telefone' para continuar!");
            jtfTelefone.requestFocus();
        } else if (jtfRg.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Preencha o campo 'RG' para continuar!");
            jtfRg.requestFocus();
        } else if (jtfCep.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Preencha o campo 'CEP' para continuar!");
            jtfCep.requestFocus();
        } else if (jtfEndereco.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Preencha o campo 'Logradouro' para continuar!");
            jtfEndereco.requestFocus();
        } else if (jtfNum.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Preencha o campo 'Número' para continuar!");
            jtfNum.requestFocus();
        } else if (jtfBairro.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Preencha o campo 'Bairro' para continuar!");
            jtfBairro.requestFocus();
        } else if (jtfCidade.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Preencha o campo 'Cidade' para continuar!");
            jtfRg.requestFocus();
        } else {
            //Dados Básicos
            policial.setNome(jtfNome.getText());
            policial.setNome_guerra(jtfNomeGuerra.getText());
            policial.setRg(jtfRg.getText());
            policial.setTelefone(jtfTelefone.getText());

            GraduacaoBean g = (GraduacaoBean) jcGraduacao.getSelectedItem();
            policial.setGraduacaoBean(g);

            FuncaoBean f = (FuncaoBean) jcFuncao.getSelectedItem();
            policial.setFuncaoBean(f);

            //Dados do Endereco
            endereco.setCep(jtfCep.getText());
            endereco.setDescricao(jtfEndereco.getText());
            endereco.setBairro(jtfBairro.getText());
            endereco.setNum(jtfNum.getText());
            endereco.setEstado((String) jcEstado.getSelectedItem());
            endereco.setCidade(jtfCidade.getText());
            policial.setCodigo((int) jtPoliciais.getValueAt(jtPoliciais.getSelectedRow(), 0));

            //Método Alterar
            policialC.alterar(endereco, policial);

            //Ações
            listarTodos();
            limparDados();
            habilitarCampos(false);
            habilitarBotoes(true, false, false, false);
        }
    }//GEN-LAST:event_btnAlterarActionPerformed

    private void btnDesativarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDesativarActionPerformed
        if (btnDesativar.getText() == "Desativar") {
            policial.setCodigo((int) jtPoliciais.getValueAt(jtPoliciais.getSelectedRow(), 0));
            policialDao.DesativarPolicial(policial);
            btnDesativar.setText("Ativar");
            listarTodos();
        } else {
            policial.setCodigo((int) jtPoliciais.getValueAt(jtPoliciais.getSelectedRow(), 0));
            policialDao.AtivarPolicial(policial);
            btnDesativar.setText("Desativar");
            listarTodos();
        }

    }//GEN-LAST:event_btnDesativarActionPerformed

    private void btnSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSairActionPerformed
        dispose();
    }//GEN-LAST:event_btnSairActionPerformed

    private void jtfNomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtfNomeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtfNomeActionPerformed

    private void jtfNomeGuerraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtfNomeGuerraActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtfNomeGuerraActionPerformed

    private void btnProximo1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProximo1ActionPerformed
        jtpDados.setSelectedIndex(1);
    }//GEN-LAST:event_btnProximo1ActionPerformed

    private void jcGraduacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcGraduacaoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jcGraduacaoActionPerformed

    private void jtfEnderecoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtfEnderecoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtfEnderecoActionPerformed

    private void jtfCepActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtfCepActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtfCepActionPerformed

    private void jtfCidadeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtfCidadeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtfCidadeActionPerformed

    private void jbBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbBuscarActionPerformed
        correio();
    }//GEN-LAST:event_jbBuscarActionPerformed

    private void btnAnterior1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnterior1ActionPerformed
        jtpDados.setSelectedIndex(0);
    }//GEN-LAST:event_btnAnterior1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAlterar;
    private javax.swing.JButton btnAnterior1;
    private javax.swing.JButton btnDesativar;
    private javax.swing.JButton btnNovo;
    private javax.swing.JButton btnPesquisar;
    private javax.swing.JToggleButton btnProximo1;
    private javax.swing.JButton btnSair;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton jbBuscar;
    private javax.swing.JComboBox jcEstado;
    private javax.swing.JComboBox jcFuncao;
    private javax.swing.JComboBox jcGraduacao;
    private javax.swing.JComboBox<String> jcbTipoPesquisa;
    private javax.swing.JTable jtPoliciais;
    private javax.swing.JTextField jtfBairro;
    private javax.swing.JTextField jtfCep;
    private javax.swing.JTextField jtfCidade;
    private javax.swing.JTextField jtfEndereco;
    private javax.swing.JTextField jtfNome;
    private javax.swing.JTextField jtfNomeGuerra;
    private javax.swing.JTextField jtfNum;
    private javax.swing.JTextField jtfPesquisa;
    private javax.swing.JTextField jtfRg;
    private javax.swing.JTextField jtfTelefone;
    private javax.swing.JTabbedPane jtpDados;
    private javax.swing.JLabel lblBairro;
    private javax.swing.JLabel lblCep;
    private javax.swing.JLabel lblCidade;
    private javax.swing.JLabel lblEndereço;
    private javax.swing.JLabel lblEstado;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblNomeGuerra;
    private javax.swing.JLabel lblNum;
    private javax.swing.JLabel lblPotoGraduacao;
    private javax.swing.JLabel lblRg;
    private javax.swing.JLabel lblTelefone;
    private javax.swing.JLabel lblTitulo;
    // End of variables declaration//GEN-END:variables

    public void correio() {
        String cep = jtfCep.getText();
        WebServiceCep webServiceCep = WebServiceCep.searchCep(cep);
        if (webServiceCep.wasSuccessful()) {
            jtfEndereco.setText(webServiceCep.getLogradouroFull());
            jtfBairro.setText(webServiceCep.getBairro());
            jtfCidade.setText(webServiceCep.getCidade());
            jcEstado.setSelectedItem(webServiceCep.getUf());
        } else {
            JOptionPane.showMessageDialog(null, webServiceCep.getResultText());
        }
    }

    public void preencherComboGraduacao(JComboBox combo) {
        GraduacaoDao graduacaoDao = new GraduacaoDao();
        try {
            List<GraduacaoBean> lista = graduacaoDao.buscarGraduacao();
            //este for e conhecido como foreach
            for (GraduacaoBean graduacao : lista) {
                combo.addItem(graduacao);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    public void preencherComboFuncao(JComboBox combo) {
        FuncaoDao funcaoDao = new FuncaoDao();
        try {
            List<FuncaoBean> lista = funcaoDao.buscarFuncao();
            //este for e conhecido como foreach
            for (FuncaoBean funcao : lista) {
                combo.addItem(funcao);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    public void listarTodos() {
        DefaultTableModel dtmPoliciais = (DefaultTableModel) jtPoliciais.getModel();
        dtmPoliciais.setNumRows(0);

        policialDao.listarTodos().forEach((policial) -> {
            dtmPoliciais.addRow(new Object[]{
                policial.getCodigo(),
                policial.getNome(),
                policial.getNome_guerra(),
                policial.getRg(),
                policial.getTelefone(),
                policial.getGraduacaoBean(),
                policial.getStatus() == 1 ? "Ativado" : "Desativado"
            });
        });
    }

    public void PesqNomeGuerra(String nome) {
        DefaultTableModel dtmArmeiros = (DefaultTableModel) jtPoliciais.getModel();
        dtmArmeiros.setNumRows(0);

        policialDao.PesqNomeGuerra(nome).forEach((policial) -> {
            dtmArmeiros.addRow(new Object[]{
                policial.getCodigo(),
                policial.getNome(),
                policial.getNome_guerra(),
                policial.getRg(),
                policial.getTelefone(),
                policial.getGraduacaoBean(),
                policial.getStatus() == 1 ? "Ativado" : "Desativado"
            });
        });
    }

    public void PesqRG(String rg) {
        DefaultTableModel dtmArmeiros = (DefaultTableModel) jtPoliciais.getModel();
        dtmArmeiros.setNumRows(0);

        policialDao.PesqRG(rg).forEach((policial) -> {
            dtmArmeiros.addRow(new Object[]{
                policial.getCodigo(),
                policial.getNome(),
                policial.getNome_guerra(),
                policial.getRg(),
                policial.getTelefone(),
                policial.getGraduacaoBean(),
                policial.getStatus() == 1 ? "Ativado" : "Desativado"
            });
        });
    }

    public void limparDados() {
        //Dados Básicos
        jtfNome.setText("");
        jtfNomeGuerra.setText("");
        jcGraduacao.setSelectedIndex(0);
        jtfTelefone.setText("");
        jtfRg.setText("");

        //Dados Endereço
        jtfCep.setText("");
        jtfEndereco.setText("");
        jtfNum.setText("");
        jtfBairro.setText("");
        jcEstado.setSelectedIndex(0);
        jtfCidade.setText("");
    }

    public void habilitarBotoes(boolean N, boolean S, boolean C, boolean D) {
        btnNovo.setEnabled(N);
        btnSalvar.setEnabled(S);
        btnAlterar.setEnabled(C);
        btnDesativar.setEnabled(D);
    }

    public void habilitarCampos(boolean valor) {
        jtpDados.setEnabled(valor);
        jtfNome.setEnabled(valor);
        jtfNomeGuerra.setEnabled(valor);
        jcGraduacao.setEnabled(valor);
        jcFuncao.setEnabled(valor);
        jtfTelefone.setEnabled(valor);
        jtfRg.setEnabled(valor);
        btnProximo1.setEnabled(valor);
    }

    public void recuperarDados() {
        PreparedStatement ps;
        ResultSet rs;
        Connection con;
        String sql;

        try {
            con = Conexao.pegarConexao();
            int row = jtPoliciais.getSelectedRow();
            String table_click = (jtPoliciais.getModel().getValueAt(row, 0).toString());
            sql = "select poli_nome, poli_nome_guerra, gradu_cod, func_cod, poli_telefone, poli_rg, "
                    + " end_cep, end_descricao, end_numero, end_bairro, end_estado, end_cidade from policial"
                    + " inner join graduacao on poli_gradu_cod = gradu_cod"
                    + " inner join funcao on poli_func_cod = func_cod"
                    + " inner join endereco on end_poli_cod = poli_cod"
                    + " where poli_cod='" + table_click + "' ";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                //Dados Básicos
                String add1 = rs.getString("poli_nome");
                jtfNome.setText(add1);
                String add2 = rs.getString("poli_nome_guerra");
                jtfNomeGuerra.setText(add2);
                int add3 = rs.getInt("gradu_cod");
                jcGraduacao.setSelectedIndex(add3);
                int add4 = rs.getInt("func_cod");
                jcFuncao.setSelectedIndex(add4);
                String add5 = rs.getString("poli_telefone");
                jtfTelefone.setText(add5);
                String add6 = rs.getString("poli_rg");
                jtfRg.setText(add6);

                //Dados Endereço
                String add7 = rs.getString("end_cep");
                jtfCep.setText(add7);
                String add8 = rs.getString("end_descricao");
                jtfEndereco.setText(add8);
                String add9 = rs.getString("end_numero");
                jtfNum.setText(add9);
                String add10 = rs.getString("end_bairro");
                jtfBairro.setText(add10);
                String add11 = rs.getString("end_estado");
                jcEstado.setSelectedItem(add11);
                String add12 = rs.getString("end_cidade");
                jtfCidade.setText(add12);

            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
        habilitarCampos(true);
        habilitarBotoes(true, false, true, true);
    }

}
