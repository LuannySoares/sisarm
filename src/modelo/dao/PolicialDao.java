package modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import modelo.bean.EnderecoBean;
import modelo.bean.GraduacaoBean;
import modelo.bean.PolicialBean;
import modelo.conexao.Conexao;

public class PolicialDao {
    
    private PreparedStatement ps;
    private ResultSet rs;
    private Connection con;
    private String sql;

    public void salvarPolicial(EnderecoBean endereco, PolicialBean policial) {
        try {
            con = Conexao.pegarConexao();
                        
            String sql1 = "INSERT INTO policial(poli_nome, poli_nome_guerra,"
                    + " poli_rg, poli_telefone, poli_gradu_cod, poli_func_cod)"
                    + " VALUES (?,?,?,?,?,?)";
            ps = con.prepareStatement(sql1);
            ps.setString(1, policial.getNome());
            ps.setString(2, policial.getNome_guerra());
            ps.setString(3, policial.getRg());
            ps.setString(4, policial.getTelefone());
            ps.setInt(5, policial.getGraduacaoBean().getCodigo());
            ps.setInt(6, policial.getFuncaoBean().getCodigo());
            ps.execute();
            
            String sql2 = "INSERT INTO endereco (end_descricao, end_cep, end_numero, end_bairro, end_estado, end_cidade, end_poli_cod) "
                    + "VALUES (?,?,?,?,?,?,(SELECT max(poli_cod) FROM policial))";
            ps = con.prepareStatement(sql2);
            ps.setString(1, endereco.getDescricao());
            ps.setString(2, endereco.getCep());
            ps.setString(3, endereco.getNum());
            ps.setString(4, endereco.getBairro());
            ps.setString(5, endereco.getEstado());
            ps.setString(6, endereco.getCidade());
            ps.execute();


            JOptionPane.showMessageDialog(null, "Policial inserido com sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao inserir policial!" + ex);
        }
        Conexao.fecharConexao();
    }

    public void alterarPolicial( EnderecoBean endereco, PolicialBean policial) {
        try {
            con = Conexao.pegarConexao();
                        
            String sql1 = "UPDATE policial SET poli_nome=?, poli_nome_guerra=?,"
                    + " poli_rg=?, poli_telefone=?, poli_gradu_cod=?, poli_func_cod=?"
                    + " WHERE poli_cod=?";
            
            ps = con.prepareStatement(sql1);
            ps.setString(1, policial.getNome());
            ps.setString(2, policial.getNome_guerra());
            ps.setString(3, policial.getRg());
            ps.setString(4, policial.getTelefone());
            ps.setInt(5, policial.getGraduacaoBean().getCodigo());
            ps.setInt(6, policial.getFuncaoBean().getCodigo());
            ps.setInt(7, policial.getCodigo());
            ps.execute();
           
            String sql2 = "UPDATE endereco SET end_descricao=?, end_cep=?, end_numero=?, end_bairro=?, end_estado=?, end_cidade=? "
                    + "WHERE end_poli_cod=(SELECT poli_cod FROM policial WHERE poli_cod=?)";
            ps = con.prepareStatement(sql2);
            ps.setString(1, endereco.getDescricao());
            ps.setString(2, endereco.getCep());
            ps.setString(3, endereco.getNum());
            ps.setString(4, endereco.getBairro());
            ps.setString(5, endereco.getEstado());
            ps.setString(6, endereco.getCidade());
            ps.setInt(7, policial.getCodigo());
            ps.execute();         
          
            JOptionPane.showMessageDialog(null, "Policial alterado com sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao alterar policial!" + ex);
            System.out.println(ex);
        }
        Conexao.fecharConexao();
    }

    public List<PolicialBean> listarTodos() {
        List<PolicialBean> lista = new ArrayList<>();

        try {
            con = Conexao.pegarConexao();
            sql = "select poli_cod, poli_nome, poli_nome_guerra, poli_rg, poli_telefone, gradu_nome, poli_status "
                    + "from policial inner join graduacao on poli_gradu_cod = gradu_cod;";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                PolicialBean policial = new PolicialBean();
                GraduacaoBean graduacao = new GraduacaoBean();
                policial.setCodigo(rs.getInt("poli_cod"));
                policial.setNome(rs.getString("poli_nome"));
                policial.setNome_guerra(rs.getString("poli_nome_guerra"));
                policial.setRg(rs.getString("poli_rg"));
                policial.setTelefone(rs.getString("poli_telefone"));
                graduacao.setNome(rs.getString("gradu_nome"));
                policial.setGraduacaoBean(graduacao);
                policial.setStatus(rs.getInt("poli_status"));
                lista.add(policial);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao listar policiais: " + ex);
        }

        Conexao.fecharConexao();
        return lista;
    }

    public List<PolicialBean> PesqNomeGuerra(String nome) {
        List<PolicialBean> lista = new ArrayList<>();

        try {
            con = Conexao.pegarConexao();
            sql = "select poli_cod, poli_nome, poli_nome_guerra, poli_rg, poli_telefone, gradu_nome, poli_status "
                    + "from policial inner join graduacao on poli_gradu_cod = gradu_cod  WHERE poli_nome_guerra LIKE ?";
            ps = con.prepareStatement(sql);
            ps.setString(1, "%" + nome + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                PolicialBean policial = new PolicialBean();
                GraduacaoBean graduacao = new GraduacaoBean();
                policial.setCodigo(rs.getInt("poli_cod"));
                policial.setNome(rs.getString("poli_nome"));
                policial.setNome_guerra(rs.getString("poli_nome_guerra"));
                policial.setRg(rs.getString("poli_rg"));
                policial.setTelefone(rs.getString("poli_telefone"));
                graduacao.setNome(rs.getString("gradu_nome"));
                policial.setGraduacaoBean(graduacao);
                policial.setStatus(rs.getInt("poli_status"));
                lista.add(policial);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao listar policiais: " + ex);
        }

        Conexao.fecharConexao();
        return lista;
    }

    public List<PolicialBean> PesqRG(String rg) {
        List<PolicialBean> lista = new ArrayList<>();

        try {
            con = Conexao.pegarConexao();
            sql = "select poli_cod, poli_nome, poli_nome_guerra, poli_rg, poli_telefone, gradu_nome, poli_status "
                    + "from policial inner join graduacao on poli_gradu_cod = gradu_cod  WHERE poli_rg LIKE ?";
            ps = con.prepareStatement(sql);
            ps.setString(1, "%" + rg + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                PolicialBean policial = new PolicialBean();
                GraduacaoBean graduacao = new GraduacaoBean();
                policial.setCodigo(rs.getInt("poli_cod"));
                policial.setNome(rs.getString("poli_nome"));
                policial.setNome_guerra(rs.getString("poli_nome_guerra"));
                policial.setRg(rs.getString("poli_rg"));
                policial.setTelefone(rs.getString("poli_telefone"));
                graduacao.setNome(rs.getString("gradu_nome"));
                policial.setGraduacaoBean(graduacao);
                policial.setStatus(rs.getInt("poli_status"));
                lista.add(policial);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao listar usuários: " + ex);
        }

        Conexao.fecharConexao();
        return lista;
    }

    public void DesativarPolicial(PolicialBean policial) {
        try {
            con = Conexao.pegarConexao();
            sql = "update policial set poli_status=0 where poli_cod=?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, policial.getCodigo());
            ps.executeUpdate();
            System.out.println(policial.getStatus());
            JOptionPane.showMessageDialog(null, "Policial desativado com sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao desativar policial!/nErro:" + ex);
        }
        Conexao.fecharConexao();
    }
    
      public void AtivarPolicial(PolicialBean policial) {
        try {
            con = Conexao.pegarConexao();
            sql = "update policial set poli_status=1 where poli_cod=?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, policial.getCodigo());
            ps.executeUpdate();
            System.out.println(policial.getStatus());
            JOptionPane.showMessageDialog(null, "Policial ativado com sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao ativar policial!/nErro:" + ex);
        }
        Conexao.fecharConexao();
    }
}
