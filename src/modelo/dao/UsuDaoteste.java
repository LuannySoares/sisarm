/*package modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import modelo.bean.UsuarioBean;
import modelo.conexao.Conexao;

public class UsuarioDao {

    private PreparedStatement ps;
    private ResultSet rs;
    private Connection con;
    private String sql;

    public UsuarioDao() {
    }

    public boolean checarLogin(String usuario, String senha) {
        boolean check = false;
        try {
            con = Conexao.pegarConexao();
            sql = "SELECT * FROM usuario WHERE usu_nome_guerra =? AND usu_senha=?";
            ps = con.prepareStatement(sql);
            ps.setString(1, usuario);
            ps.setString(2, senha);
            rs = ps.executeQuery();
            if (rs.next()) {
                check = true;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao listar usuários: " + ex);
        }

        Conexao.fecharConexao();
        return check;
    }

    public void salvarUsuario(UsuarioBean usuario) {
        try {
            con = Conexao.pegarConexao();
            sql = "INSERT INTO usuario(usu_nome_login, usu_senha) VALUES (?,?)";
            ps = con.prepareStatement(sql);
            ps.setString(1, usuario.getLogin());
            ps.setString(2, usuario.getSenha());
            ps.execute();
            Conexao.pegarConexao().commit();
            JOptionPane.showMessageDialog(null, "Usuário inserido com sucesso!", "Suceso", 1, new ImageIcon("imagens/dbok.png"));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex, "Erro", 0, new ImageIcon("imagens/dberro.png"));
        }
       
 Conexao.fecharConexao();
    }

    public void AlterarUsuario(UsuarioBean usuario) {
        try {
            con = Conexao.pegarConexao();
            sql = "UPDATE usuario SET usu_login=?, usu_senha=?, usu_tipo=? WHERE usu_cod=?";
            ps = con.prepareStatement(sql);
            ps.setString(1, usuario.getNome_guerra());
            ps.setString(2, usuario.getSenha());
            ps.setString(3, usuario.getTipo());
            ps.setInt(4, usuario.getCodigo());
            ps.execute();
            Conexao.pegarConexao().commit();
            JOptionPane.showMessageDialog(null, "Usuário alterado com sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao alterar usuário!/nErro:" + ex);
        }
        Conexao.fecharConexao();
    }

    public List<UsuarioBean> listarTodos() {
        List<UsuarioBean> usuarios = new ArrayList<>();

        try {
            con = Conexao.pegarConexao();
            sql = "select * from usuario";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                UsuarioBean user = new UsuarioBean();
                user.setCodigo(rs.getInt("usu_cod"));
                user.setNome_guerra(rs.getString("usu_nome_guerra"));
                user.setSenha(rs.getString("usu_senha"));
                user.setTipo(rs.getString("usu_tipo"));
                usuarios.add(user);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao listar usuários: " + ex);
        }

        Conexao.fecharConexao();
        return usuarios;
    }

    public List<UsuarioBean> listarPorNome(String nome) {
        List<UsuarioBean> usuarios = new ArrayList<>();

        try {
            con = Conexao.pegarConexao();
            sql = "SELECT * FROM usuario WHERE usu_nome_guerra LIKE ?";
            ps = con.prepareStatement(sql);
            ps.setString(1, "%" + nome + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                UsuarioBean user = new UsuarioBean();
                user.setCodigo(rs.getInt("usu_cod"));
                user.setNome_guerra(rs.getString("usu_nome_guerra"));
                user.setSenha(rs.getString("usu_senha"));
                user.setTipo(rs.getString("usu_tipo"));
                usuarios.add(user);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao listar usuários: " + ex);
        }

        Conexao.fecharConexao();
        return usuarios;
    }

    public List<UsuarioBean> listarPorTipo(String tipo) {
        List<UsuarioBean> usuarios = new ArrayList<>();

        try {
            con = Conexao.pegarConexao();
            sql = "SELECT * FROM usuario WHERE usu_tipo LIKE ?";
            ps = con.prepareStatement(sql);
            ps.setString(1, "%" + tipo + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                UsuarioBean user = new UsuarioBean();
                user.setCodigo(rs.getInt("usu_cod"));
                user.setNome_guerra(rs.getString("usu_nome_guerra"));
                user.setSenha(rs.getString("usu_senha"));
                user.setTipo(rs.getString("usu_tipo"));
                usuarios.add(user);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao listar usuários: " + ex);
        }

        Conexao.fecharConexao();
        return usuarios;
    }

    public void ExcluirUsuario(UsuarioBean user) {
        try {
            con = Conexao.pegarConexao();
            sql = "DELETE FROM usuario WHERE usu_cod=?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, user.getCodigo());
            ps.execute();
            Conexao.pegarConexao().commit();
            JOptionPane.showMessageDialog(null, "Usuário excluído com sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao excluir usuário!/nErro:" + ex);
        }
        Conexao.fecharConexao();
    }
}
*/