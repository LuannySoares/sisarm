package modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import modelo.bean.ArmeiroBean;
import modelo.bean.EnderecoBean;
import modelo.bean.GraduacaoBean;
import modelo.conexao.Conexao;

public class ArmeiroDao {

    private PreparedStatement ps;
    private ResultSet rs;
    private Connection con;
    private String sql;

    public void salvarArmeiro(EnderecoBean endereco, ArmeiroBean armeiro) {
        try {
            con = Conexao.pegarConexao();

            String sql1 = "INSERT INTO policial(poli_nome, poli_nome_guerra,"
                    + " poli_rg, poli_telefone, poli_gradu_cod, poli_func_cod)"
                    + " VALUES (?,?,?,?,?,?)";
            ps = con.prepareStatement(sql1);
            ps.setString(1, armeiro.getNome());
            ps.setString(2, armeiro.getNome_guerra());
            ps.setString(3, armeiro.getRg());
            ps.setString(4, armeiro.getTelefone());
            ps.setInt(5, armeiro.getGraduacaoBean().getCodigo());
            ps.setInt(6, armeiro.getFuncaoBean().getCodigo());
            ps.execute();
            
            
            String sql2 = "INSERT INTO endereco (end_descricao, end_cep, end_numero, end_bairro, end_estado, end_cidade, end_poli_cod) "
                    + "VALUES (?,?,?,?,?,?,(SELECT max(poli_cod) FROM policial))";
            ps = con.prepareStatement(sql2);
            ps.setString(1, endereco.getDescricao());
            ps.setString(2, endereco.getCep());
            ps.setString(3, endereco.getNum());
            ps.setString(4, endereco.getBairro());
            ps.setString(5, endereco.getEstado());
            ps.setString(6, endereco.getCidade());
            ps.execute();

            String sql3 = "INSERT INTO armeiro(arm_poli_cod, arm_login, arm_senha) "
                    + "VALUES ((SELECT max(poli_cod) FROM policial),?,?)";
            ps = con.prepareStatement(sql3);
            ps.setString(1, armeiro.getLogin());
            ps.setString(2, armeiro.getSenha());
            ps.execute();

            JOptionPane.showMessageDialog(null, "Armeiro inserido com sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao inserir armeiro!" + ex);
        }
        Conexao.fecharConexao();
    }

    public void alterarArmeiro(EnderecoBean endereco, ArmeiroBean armeiro) {
        try {
            con = Conexao.pegarConexao();
            String sql1 = "UPDATE policial SET poli_nome=?, poli_nome_guerra=?,"
                    + " poli_rg=?, poli_telefone=?, poli_gradu_cod=?, poli_func_cod=?"
                    + " WHERE poli_cod=?";

            ps = con.prepareStatement(sql1);
            ps.setString(1, armeiro.getNome());
            ps.setString(2, armeiro.getNome_guerra());
            ps.setString(3, armeiro.getRg());
            ps.setString(4, armeiro.getTelefone());
            ps.setInt(5, armeiro.getGraduacaoBean().getCodigo());
            ps.setInt(6, armeiro.getFuncaoBean().getCodigo());
            ps.setInt(7, armeiro.getCodigo());
            ps.execute();
            
            String sql2 = "UPDATE endereco SET end_descricao=?, end_cep=?, end_numero=?, end_bairro=?, end_estado=?, end_cidade=? "
                    + "WHERE end_poli_cod=(SELECT poli_cod FROM policial WHERE poli_cod=?)";
            ps = con.prepareStatement(sql2);
            ps.setString(1, endereco.getDescricao());
            ps.setString(2, endereco.getCep());
            ps.setString(3, endereco.getNum());
            ps.setString(4, endereco.getBairro());
            ps.setString(5, endereco.getEstado());
            ps.setString(6, endereco.getCidade());
            ps.setInt(7, armeiro.getCodigo());
            ps.execute();
                      
            String sql3 = "UPDATE armeiro SET arm_login=?, arm_senha=? WHERE arm_poli_cod=(SELECT poli_cod FROM policial WHERE poli_cod=?)";
            ps = con.prepareStatement(sql3);
            ps.setString(1, armeiro.getLogin());
            ps.setString(2, armeiro.getSenha());
            ps.setInt(3, armeiro.getCodigo());
            ps.execute();

            JOptionPane.showMessageDialog(null, "Usuário alterado com sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao alterar usuário!" + ex);
            System.out.println(ex);
        }
        Conexao.fecharConexao();
    }

    public List<ArmeiroBean> listarTodos() {
        List<ArmeiroBean> lista = new ArrayList<>();

        try {
            con = Conexao.pegarConexao();
            sql = "select poli_cod, poli_nome, poli_nome_guerra, poli_rg, poli_telefone, gradu_nome, poli_status "
                    + "from policial inner join graduacao on poli_gradu_cod = gradu_cod "
                    + " inner join funcao on poli_func_cod = func_cod WHERE func_descricao='Armeiro';";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                ArmeiroBean armeiro = new ArmeiroBean();
                GraduacaoBean graduacao = new GraduacaoBean();
                armeiro.setCodigo(rs.getInt("poli_cod"));
                armeiro.setNome(rs.getString("poli_nome"));
                armeiro.setNome_guerra(rs.getString("poli_nome_guerra"));
                armeiro.setRg(rs.getString("poli_rg"));
                armeiro.setTelefone(rs.getString("poli_telefone"));
                graduacao.setNome(rs.getString("gradu_nome"));
                armeiro.setGraduacaoBean(graduacao);
                armeiro.setStatus(rs.getInt("poli_status"));
                lista.add(armeiro);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao listar usuários: " + ex);
        }

        Conexao.fecharConexao();
        return lista;
    }

    public List<ArmeiroBean> PesqNomeGuerra(String nome) {
        List<ArmeiroBean> lista = new ArrayList<>();

        try {
            con = Conexao.pegarConexao();
            sql = "select poli_cod, poli_nome, poli_nome_guerra, poli_rg, poli_telefone, gradu_nome, poli_status "
                    + "from policial inner join graduacao on poli_gradu_cod = gradu_cod  WHERE poli_nome_guerra LIKE ?";
            ps = con.prepareStatement(sql);
            ps.setString(1, "%" + nome + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                ArmeiroBean armeiro = new ArmeiroBean();
                GraduacaoBean graduacao = new GraduacaoBean();
                armeiro.setCodigo(rs.getInt("poli_cod"));
                armeiro.setNome(rs.getString("poli_nome"));
                armeiro.setNome_guerra(rs.getString("poli_nome_guerra"));
                armeiro.setRg(rs.getString("poli_rg"));
                armeiro.setTelefone(rs.getString("poli_telefone"));
                graduacao.setNome(rs.getString("gradu_nome"));
                armeiro.setGraduacaoBean(graduacao);
                armeiro.setStatus(rs.getInt("poli_status"));
                lista.add(armeiro);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao listar usuários: " + ex);
        }

        Conexao.fecharConexao();
        return lista;
    }

    public List<ArmeiroBean> PesqRG(String rg) {
        List<ArmeiroBean> lista = new ArrayList<>();

        try {
            con = Conexao.pegarConexao();
            sql = "select poli_cod, poli_nome, poli_nome_guerra, poli_rg, poli_telefone, gradu_nome, poli_status "
                    + "from policial inner join graduacao on poli_gradu_cod = gradu_cod  WHERE poli_rg LIKE ?";
            ps = con.prepareStatement(sql);
            ps.setString(1, "%" + rg + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                ArmeiroBean armeiro = new ArmeiroBean();
                GraduacaoBean graduacao = new GraduacaoBean();
                armeiro.setCodigo(rs.getInt("poli_cod"));
                armeiro.setNome(rs.getString("poli_nome"));
                armeiro.setNome_guerra(rs.getString("poli_nome_guerra"));
                armeiro.setRg(rs.getString("poli_rg"));
                armeiro.setTelefone(rs.getString("poli_telefone"));
                graduacao.setNome(rs.getString("gradu_nome"));
                armeiro.setGraduacaoBean(graduacao);
                armeiro.setStatus(rs.getInt("poli_status"));
                lista.add(armeiro);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao listar usuários: " + ex);
        }

        Conexao.fecharConexao();
        return lista;
    }

    public void DesativarArmeiro(ArmeiroBean armeiro) {
        try {
            con = Conexao.pegarConexao();
            sql = "update policial set poli_status=0 where poli_cod=?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, armeiro.getCodigo());
            ps.executeUpdate();
            System.out.println(armeiro.getStatus());
            JOptionPane.showMessageDialog(null, "Armeiro desativado com sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao desativar armeiro!/nErro:" + ex);
        }
        Conexao.fecharConexao();
    }

    public void AtivarArmeiro(ArmeiroBean armeiro) {
        try {
            con = Conexao.pegarConexao();
            sql = "update policial set poli_status=1 where poli_cod=?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, armeiro.getCodigo());
            ps.executeUpdate();
            System.out.println(armeiro.getStatus());
            JOptionPane.showMessageDialog(null, "Armeiro ativado com sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao ativar armeiro!/nErro:" + ex);
        }
        Conexao.fecharConexao();
    }
}
