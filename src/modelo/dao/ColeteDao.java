package modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import modelo.bean.ColeteBean;
import modelo.bean.MunicaoBean;
import modelo.conexao.Conexao;

public class ColeteDao {

    private PreparedStatement ps;
    private ResultSet rs;
    private Connection con;
    private String sql;

    public void cadastrarColete(ColeteBean colete) {
        try {
            con = Conexao.pegarConexao();

            String sql1 = "insert into equipamento(equ_descricao, equ_qtd) values(?,?)";
            ps = con.prepareStatement(sql1);
            ps.setString(1, colete.getDescricao());
            ps.setInt(2, colete.getQuantidade());
            ps.execute();

            String sql2 = "insert into colete(col_equ_cod, col_tombo, col_fabricacao, col_validade, col_tipo, col_tamanho)"
                    + " values((SELECT max(equ_cod) FROM equipamento),?,?,?,?,?)";
            ps = con.prepareStatement(sql2);
            ps.setInt(1, colete.getTombo());
            ps.setDate(2, colete.getFabricacao());
            ps.setDate(3, colete.getValidade());
            ps.setString(4, colete.getTipo());
            ps.setString(5, colete.getTamanho());
            ps.execute();
            Conexao.fecharConexao();
            JOptionPane.showMessageDialog(null, "Colete inserido com sucesso!");
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro ao cadastrar colete" + e);
        }
    }

    public void alterarColete(ColeteBean colete) {
        try {
            con = Conexao.pegarConexao();

            String sql1 = "update equipamento SET equ_descricao=?, equ_qtd=? WHERE equ_cod=?";
            ps = con.prepareStatement(sql1);
            ps.setString(1, colete.getDescricao());
            ps.setInt(2, colete.getQuantidade());
            ps.setInt(3, colete.getCodigo());
            ps.execute();

            String sql2 = "update colete SET col_tombo=?, col_fabricacao=?, col_validade=?, col_tipo=?, col_tamanho=?"
                    + " WHERE col_equ_cod=(SELECT equ_cod FROM equipamento WHERE equ_cod=?)";
            ps = con.prepareStatement(sql2);
            ps.setInt(1, colete.getTombo());
            ps.setDate(2, colete.getFabricacao());
            ps.setDate(3, colete.getValidade());
            ps.setString(4, colete.getTipo());
            ps.setString(5, colete.getTamanho());
            ps.setInt(6, colete.getCodigo());
            ps.execute();
            Conexao.fecharConexao();
            JOptionPane.showMessageDialog(null, "Colete inserido com sucesso!");
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro ao cadastrar colete" + e);
        }
    }

    public List<ColeteBean> listarTodos() {
        List<ColeteBean> lista = new ArrayList<>();

        try {
            con = Conexao.pegarConexao();
            sql = "select equ_cod, equ_descricao, col_tombo, col_tipo, col_tamanho, equ_status from equipamento"
                    + " inner join colete on col_equ_cod = equ_cod;";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                ColeteBean colete = new ColeteBean();
                colete.setCodigo(rs.getInt("equ_cod"));
                colete.setDescricao(rs.getString("equ_descricao"));
                colete.setTombo(rs.getInt("col_tombo"));
                colete.setTipo(rs.getString("col_tipo"));
                colete.setTamanho(rs.getString("col_tamanho"));
                colete.setStatus(rs.getInt("equ_status"));
                lista.add(colete);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao listar munições: " + ex);
        }

        Conexao.fecharConexao();
        return lista;
    }

    public List<ColeteBean> listarPorNome(String nome) {
        List<ColeteBean> lista = new ArrayList<>();

        try {
            con = Conexao.pegarConexao();
            sql = "select equ_cod, equ_descricao, col_tombo, col_tipo, col_tamanho, equ_status from equipamento"
                    + " inner join colete on col_equ_cod = equ_cod WHERE equ_descricao LIKE ?;";
            ps = con.prepareStatement(sql);
            ps.setString(1, "%" + nome + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                ColeteBean colete = new ColeteBean();
                colete.setCodigo(rs.getInt("equ_cod"));
                colete.setDescricao(rs.getString("equ_descricao"));
                colete.setTombo(rs.getInt("col_tombo"));
                colete.setTipo(rs.getString("col_tipo"));
                colete.setTamanho(rs.getString("col_tamanho"));
                colete.setStatus(rs.getInt("equ_status"));
                lista.add(colete);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao listar munições: " + ex);
        }
        Conexao.fecharConexao();
        return lista;
    }

    public List<ColeteBean> listarPorTipo(String tipo) {
        List<ColeteBean> lista = new ArrayList<>();

        try {
            con = Conexao.pegarConexao();
            sql = "select equ_cod, equ_descricao, col_tombo, col_tipo, col_tamanho, equ_status from equipamento"
                    + " inner join colete on col_equ_cod = equ_cod WHERE col_tombo LIKE ?;";
            ps = con.prepareStatement(sql);
            ps.setString(1, "%" + tipo + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                ColeteBean colete = new ColeteBean();
                colete.setCodigo(rs.getInt("equ_cod"));
                colete.setDescricao(rs.getString("equ_descricao"));
                colete.setTombo(rs.getInt("col_tombo"));
                colete.setTipo(rs.getString("col_tipo"));
                colete.setTamanho(rs.getString("col_tamanho"));
                colete.setStatus(rs.getInt("equ_status"));
                lista.add(colete);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao listar munições: " + ex);
        }
        Conexao.fecharConexao();
        return lista;
    }

    public void excluirColete(ColeteBean colete) {
        try {
            con = Conexao.pegarConexao();
            sql = "delete from equipamento where equ_cod = ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, colete.getCodigo());
            ps.execute();
            JOptionPane.showMessageDialog(null, "Munição excluída com sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao excluir munição: " + ex);
        }

        Conexao.fecharConexao();
    }//fim do metodo excluir

}
