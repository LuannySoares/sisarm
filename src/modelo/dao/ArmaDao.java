package modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import modelo.bean.ArmaBean;
import modelo.conexao.Conexao;

public class ArmaDao {

    private PreparedStatement ps;
    private ResultSet rs;
    private Connection con;
    private String sql;

    public void salvarArma(ArmaBean arma) {
        try {
            con = Conexao.pegarConexao();

            String sql1 = "insert into equipamento(equ_descricao, equ_qtd) values(?,?)";
            ps = con.prepareStatement(sql1);
            ps.setString(1, arma.getDescricao());
            ps.setInt(2, arma.getQuantidade());
            ps.execute();

            String sql2 = "insert into arma(arm_equ_cod, arm_marca, arm_modelo, arm_calibre, arm_num_serie)"
                    + " values((SELECT max(equ_cod) FROM equipamento),?,?,?,?)";
            ps = con.prepareStatement(sql2);
            ps.setString(1, arma.getMarca());
            ps.setString(2, arma.getModelo());
            ps.setString(3, arma.getCalibre());
            ps.setString(4, arma.getNum_serie());
            ps.execute();
            Conexao.fecharConexao();
            JOptionPane.showMessageDialog(null, "Arma inserida com sucesso!");
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro ao cadastrar arma" + e);
        }
    }

    public void alterarArma(ArmaBean arma) {
        try {
            con = Conexao.pegarConexao();

            String sql1 = "update equipamento SET equ_descricao=?, equ_qtd=? WHERE equ_cod=?";
            ps = con.prepareStatement(sql1);
            ps.setString(1, arma.getDescricao());
            ps.setInt(2, arma.getQuantidade());
            ps.setInt(3, arma.getCodigo());
            ps.execute();

            String sql2 = "UPDATE arma SET arm_marca=?, arm_modelo=?, arm_calibre=?, arm_num_serie=?"
                    + "WHERE arm_equ_cod=(SELECT equ_cod FROM equipamento WHERE equ_cod=?)";
            ps = con.prepareStatement(sql2);
            ps.setString(1, arma.getMarca());
            ps.setString(2, arma.getModelo());
            ps.setString(3, arma.getCalibre());
            ps.setString(4, arma.getNum_serie());
            ps.setInt(5, arma.getCodigo());
            ps.execute();
            Conexao.fecharConexao();
            JOptionPane.showMessageDialog(null, "Arma alterada com sucesso!");
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro ao alterar arma" + e);
        }
    }

    public List<ArmaBean> listarTodas() {
        List<ArmaBean> lista = new ArrayList<>();

        try {
            con = Conexao.pegarConexao();
            sql = "select equ_cod, equ_descricao, arm_marca, arm_modelo, arm_calibre, equ_status from equipamento"
                    + " inner join arma on arm_equ_cod = equ_cod;";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                ArmaBean arma = new ArmaBean();
                arma.setCodigo(rs.getInt("equ_cod"));
                arma.setDescricao(rs.getString("equ_descricao"));
                arma.setMarca(rs.getString("arm_marca"));
                arma.setModelo(rs.getString("arm_modelo"));
                arma.setCalibre(rs.getString("arm_calibre"));
                arma.setStatus(rs.getInt("equ_status"));
                lista.add(arma);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao listar usuários: " + ex);
        }

        Conexao.fecharConexao();
        return lista;
    }

    public List<ArmaBean> listarPorNome(String nome) {
        List<ArmaBean> lista = new ArrayList<>();

        try {
            con = Conexao.pegarConexao();
            sql = "select equ_cod, equ_descricao, arm_marca, arm_modelo, arm_calibre, equ_status from equipamento"
                    + " inner join arma on arm_equ_cod = equ_cod WHERE equ_descricao LIKE ?;  ";
            ps = con.prepareStatement(sql);
            ps.setString(1, "%" + nome + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                ArmaBean arma = new ArmaBean();
                arma.setCodigo(rs.getInt("equ_cod"));
                arma.setDescricao(rs.getString("equ_descricao"));
                arma.setMarca(rs.getString("arm_marca"));
                arma.setModelo(rs.getString("arm_modelo"));
                arma.setCalibre(rs.getString("arm_calibre"));
                arma.setStatus(rs.getInt("equ_status"));
                lista.add(arma);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao listar usuários: " + ex);
        }

        Conexao.fecharConexao();
        return lista;
    }

    public List<ArmaBean> listarPorCalibre(String calibre) {
        List<ArmaBean> lista = new ArrayList<>();

        try {
            con = Conexao.pegarConexao();
            sql = "select equ_cod, equ_descricao, arm_marca, arm_modelo, arm_calibre, equ_status from equipamento"
                    + " inner join arma on arm_equ_cod = equ_cod WHERE arm_calibre LIKE ?;  ";
            ps = con.prepareStatement(sql);
            ps.setString(1, "%" + calibre + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                ArmaBean arma = new ArmaBean();
                arma.setCodigo(rs.getInt("equ_cod"));
                arma.setDescricao(rs.getString("equ_descricao"));
                arma.setMarca(rs.getString("arm_marca"));
                arma.setModelo(rs.getString("arm_modelo"));
                arma.setCalibre(rs.getString("arm_calibre"));
                arma.setStatus(rs.getInt("equ_status"));
                lista.add(arma);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao listar usuários: " + ex);
        }

        Conexao.fecharConexao();
        return lista;
    }

    public void excluirArma(ArmaBean arma) {
        try {
            con = Conexao.pegarConexao();
            sql = "delete from equipamento where equ_cod = ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, arma.getCodigo());
            ps.execute();
            JOptionPane.showMessageDialog(null, "Arma excluída com sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao excluir arma: " + ex);
        }

        Conexao.fecharConexao();
    }//fim do metodo excluir

}
