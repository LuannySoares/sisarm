package modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modelo.bean.FuncaoBean;
import modelo.conexao.Conexao;

public class FuncaoDao {
    private PreparedStatement ps;
    private ResultSet rs;
    private Connection con;
    private String sql;

    public List<FuncaoBean> buscarFuncao() throws SQLException {
        con = Conexao.pegarConexao();
        sql = "select * from funcao;";
        ps = con.prepareStatement(sql);
        rs = ps.executeQuery();
        List<FuncaoBean> lista = new ArrayList<>();
        while (rs.next()) {
            FuncaoBean funcao = new FuncaoBean();
            funcao.setCodigo(rs.getInt("func_cod"));
            funcao.setNome(rs.getString("func_descricao"));
            lista.add(funcao);
        }
        return lista;
    }

}
