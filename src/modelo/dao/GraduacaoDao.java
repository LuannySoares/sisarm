package modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modelo.bean.GraduacaoBean;
import modelo.conexao.Conexao;

public class GraduacaoDao {

    private PreparedStatement ps;
    private ResultSet rs;
    private Connection con;
    private String sql;

    public List<GraduacaoBean> buscarGraduacao() throws SQLException {
        con = Conexao.pegarConexao();
        sql = "select * from graduacao;";
        ps = con.prepareStatement(sql);
        rs = ps.executeQuery();
        List<GraduacaoBean> lista = new ArrayList<>();
        while (rs.next()) {
            GraduacaoBean graduacao = new GraduacaoBean();
            graduacao.setCodigo(rs.getInt("gradu_cod"));
            graduacao.setNome(rs.getString("gradu_nome"));
            lista.add(graduacao);
        }
        return lista;
    }

}
