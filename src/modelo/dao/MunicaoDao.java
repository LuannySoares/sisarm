package modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import modelo.bean.ArmaBean;
import modelo.bean.MunicaoBean;
import modelo.conexao.Conexao;

public class MunicaoDao {
    private PreparedStatement ps;
    private ResultSet rs;
    private Connection con;
    private String sql;

    public void salvarMunicao(MunicaoBean municao) {
        try {
            con = Conexao.pegarConexao();

            String sql1 = "insert into equipamento(equ_descricao, equ_qtd) values(?,?)";
            ps = con.prepareStatement(sql1);
            ps.setString(1, municao.getDescricao());
            ps.setInt(2, municao.getQuantidade());
            ps.execute();

            String sql2 = "insert into municao(mun_equ_cod, mun_marca, mun_calibre)"
                    + " values((SELECT max(equ_cod) FROM equipamento),?,?)";
            ps = con.prepareStatement(sql2);
            ps.setString(1, municao.getMarca());
            ps.setString(2, municao.getCalibre());
            ps.execute();
            Conexao.fecharConexao();
            JOptionPane.showMessageDialog(null, "Arma inserida com sucesso!");
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro ao cadastrar arma" + e);
        }
    }

    public void alterarMunicao(MunicaoBean municao) {
        try {
            con = Conexao.pegarConexao();

            String sql1 = "update equipamento SET equ_descricao=?, equ_qtd=? WHERE equ_cod=?";
            ps = con.prepareStatement(sql1);
            ps.setString(1, municao.getDescricao());
            ps.setInt(2, municao.getQuantidade());
            ps.setInt(3, municao.getCodigo());
            ps.execute();

            String sql2 = "UPDATE municao SET mun_marca=?, mun_calibre=?"
                    + " WHERE mun_equ_cod=(SELECT equ_cod FROM equipamento WHERE equ_cod=?)";
            ps = con.prepareStatement(sql2);
            ps.setString(1, municao.getMarca());
            ps.setString(2, municao.getCalibre());
            ps.setInt(3, municao.getCodigo());
            ps.execute();
            Conexao.fecharConexao();
            JOptionPane.showMessageDialog(null, "Munição alterada com sucesso!");
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro ao alterar munição" + e);
        }
    }

    public List<MunicaoBean> listarTodas() {
        List<MunicaoBean> lista = new ArrayList<>();

        try {
            con = Conexao.pegarConexao();
            sql = "select equ_cod, equ_descricao, equ_status, mun_marca, mun_calibre from equipamento"
                    + " inner join municao on mun_equ_cod = equ_cod;";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                MunicaoBean municao = new MunicaoBean();
                municao.setCodigo(rs.getInt("equ_cod"));
                municao.setDescricao(rs.getString("equ_descricao"));
                municao.setMarca(rs.getString("mun_marca"));
                municao.setCalibre(rs.getString("mun_calibre"));
                municao.setStatus(rs.getInt("equ_status"));
                lista.add(municao);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao listar munições: " + ex);
        }

        Conexao.fecharConexao();
        return lista;
    }

    public List<MunicaoBean> listarPorCalibre(String calibre) {
        List<MunicaoBean> lista = new ArrayList<>();

        try {
            con = Conexao.pegarConexao();
            sql = "select equ_cod, equ_descricao, equ_status, mun_marca, mun_calibre from equipamento"
                    + " inner join municao on mun_equ_cod = equ_cod WHERE mun_calibre LIKE ?;  ";
            ps = con.prepareStatement(sql);
            ps.setString(1, "%" + calibre + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                MunicaoBean municao = new MunicaoBean();
                municao.setCodigo(rs.getInt("equ_cod"));
                municao.setDescricao(rs.getString("equ_descricao"));
                municao.setMarca(rs.getString("mun_marca"));
                municao.setCalibre(rs.getString("mun_calibre"));
                municao.setStatus(rs.getInt("equ_status"));
                lista.add(municao);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao listar munições: " + ex);
        }

        Conexao.fecharConexao();
        return lista;
    }
    
    public List<MunicaoBean> listarPorNome (String nome) {
        List<MunicaoBean> lista = new ArrayList<>();

        try {
            con = Conexao.pegarConexao();
            sql = "select equ_cod, equ_descricao, equ_status, mun_marca, mun_calibre from equipamento"
                    + " inner join municao on mun_equ_cod = equ_cod WHERE mun_calibre LIKE ?;  ";
            ps = con.prepareStatement(sql);
            ps.setString(1, "%" + nome + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                MunicaoBean municao = new MunicaoBean();
                municao.setCodigo(rs.getInt("equ_cod"));
                municao.setDescricao(rs.getString("equ_descricao"));
                municao.setMarca(rs.getString("mun_marca"));
                municao.setCalibre(rs.getString("mun_calibre"));
                municao.setStatus(rs.getInt("equ_status"));
                lista.add(municao);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao listar munições: " + ex);
        }

        Conexao.fecharConexao();
        return lista;
    }

    public void excluirMunicao(MunicaoBean municao) {
        try {
            con = Conexao.pegarConexao();
            sql = "delete from equipamento where equ_cod = ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, municao.getCodigo());
            ps.execute();
            JOptionPane.showMessageDialog(null, "Munição excluída com sucesso!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao excluir munição: " + ex);
        }

        Conexao.fecharConexao();
    }//fim do metodo excluir
}
