package modelo.conexao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class Conexao {
    private static final String USUARIO = "root";
    private  static final String SENHA = "root";
    private static final String URL = "jdbc:mysql://localhost:3306/locacao";
    private static Connection conecta = null;
    private static java.sql.Connection con;
    public Statement stm;
    public ResultSet rs;

 public static Connection pegarConexao(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conecta = DriverManager.getConnection(URL, USUARIO, SENHA);
            System.out.println("Conectado com sucesso");
        } catch (ClassNotFoundException ex) {
            System.out.println("Classe nao encontrada "+ex.getMessage());
        } catch (SQLException ex) {
            System.out.println("Erro ao conecta com mysql "+ex.getMessage());
        }
        return conecta;
    }

    public void executaSQL(String sql) {
        try {
            stm = con.createStatement(rs.TYPE_SCROLL_INSENSITIVE, rs.CONCUR_READ_ONLY);
            rs = stm.executeQuery(sql);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro de execução SQL!" + ex.getMessage());
        }
    }

 
     public static void fecharConexao(){
        try {
            conecta.close();
            System.out.println("Conxao fechada");
        } catch (SQLException ex) {
            System.out.println("Erro ao fechar conexao "+ex.getMessage());
        }
    }//fim

}
