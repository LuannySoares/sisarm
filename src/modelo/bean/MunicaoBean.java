package modelo.bean;

public class MunicaoBean extends EquipamentoBean{
    private String marca;
    private String calibre;

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getCalibre() {
        return calibre;
    }

    public void setCalibre(String calibre) {
        this.calibre = calibre;
    }
}
