package modelo.bean;

public class PolicialBean{
    protected int codigo;
    protected String nome;
    protected String nome_guerra;
    protected String rg;
    protected String telefone;
    protected int status;
    protected EnderecoBean enderecoBean;
    protected GraduacaoBean graduacaoBean;
    protected FuncaoBean funcaoBean;        

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNome_guerra() {
        return nome_guerra;
    }

    public void setNome_guerra(String nome_guerra) {
        this.nome_guerra = nome_guerra;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public EnderecoBean getEnderecoBean() {
        return enderecoBean;
    }

    public void setEnderecoBean(EnderecoBean enderecoBean) {
        this.enderecoBean = enderecoBean;
    }

    public GraduacaoBean getGraduacaoBean() {
        return graduacaoBean;
    }

    public void setGraduacaoBean(GraduacaoBean graduacaoBean) {
        this.graduacaoBean = graduacaoBean;
    }

    public FuncaoBean getFuncaoBean() {
        return funcaoBean;
    }

    public void setFuncaoBean(FuncaoBean funcaoBean) {
        this.funcaoBean = funcaoBean;
    }   
    
}
