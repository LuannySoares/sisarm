package controle;

import java.util.ArrayList;
import modelo.bean.EnderecoBean;
import modelo.bean.PolicialBean;
import modelo.dao.PolicialDao;

public class PolicialControle {
      PolicialDao policialDao;
    
    public PolicialControle(){
        policialDao = new PolicialDao();
    }
    
    public void salvar(EnderecoBean endereco, PolicialBean policial){
        policialDao.salvarPolicial(endereco, policial);
    }
    
    public void alterar(EnderecoBean endereco, PolicialBean policial){
        policialDao.alterarPolicial(endereco, policial);
    }
    
    public ArrayList<PolicialBean> ListarTodos(){
        return (ArrayList<PolicialBean>) policialDao.listarTodos();
    }
      
    public void desativar(PolicialBean policial){
        policialDao.DesativarPolicial(policial);
    }
    
    public void ativar( PolicialBean policial){
        policialDao.AtivarPolicial(policial);
    }
}
